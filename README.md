# vVoteLibrary #
The vVoteLibrary provides utility functions for the various different components in the system. It is cross-compiled for Android and incorporated into the Android based apps as well as being used by all the Surrey developed back-end components.

## Key Functionality ##
* HTTP Comms
* Message Handling
* Fields Constants (for messages)
* Crypto Utils
* Comparators
* Signature Wrapper to handle both DSA and BLS Signatures
* BLS KeyStore and Key handling
* IO Utils - including BoundedBufferedInputStream
* FileSystemWatcher
* CRL Handling