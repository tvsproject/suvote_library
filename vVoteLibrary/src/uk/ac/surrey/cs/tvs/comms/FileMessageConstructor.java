/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Utility class for creating submissions for FileMessages to the MBB. It is intended this class could be called by external
 * libraries (ximix) to upload files to the MBB. It can be combined with the MBB class to send a a file to the MBB.
 * 
 * @author Chris Culnane
 * 
 */
public class FileMessageConstructor {

  /**
   * Message type for file messages
   */
  private static final String MESSAGE_TYPE = "file";

  /**
   * Creates a new FileMessage with a randomly generated submissionID obtained from the UUID class. The underlying call is to the
   * create method with an explicit messageID value.
   * 
   * Create a new JSON message object, sets the various field (type, id, description). It calculates the digest of the submitted
   * file and the filesize and sets them as fields in the message object. A base64 encoded copy of the digest is also included in
   * the sender signature, along with the messageID. The signature is signed by the private key in KeyStore that matches the
   * keyEntity provided. The senderID is the system wide unique ID for the sender and should match the name associated with its
   * certificate. This value will be used by the receiver to check the sender signature. Note: the MBB will only accept file
   * submissions from devices that have their public key in the FileMessage certificate store, otherwise it will be rejected.
   * 
   * @param file
   *          File to send
   * @param keyStore
   *          TVSKeyStore containing a BLS private key for the keyEntity
   * @param keyEntity
   *          String of the name of the entity to retrieve the BLS key for
   * @param senderID
   *          String of the system wide unique ID for this sender
   * @param description
   *          String description of what is being sent - mainly used for logging
   * @return JSONObject of the constructed message
   * @throws NoSuchAlgorithmException
   * @throws TVSKeyStoreException
   * @throws TVSSignatureException
   * @throws JSONException
   * @throws IOException
   */
  public static JSONObject create(File file, TVSKeyStore keyStore, String keyEntity, String senderID, String description)
      throws TVSKeyStoreException, NoSuchAlgorithmException, TVSSignatureException, JSONException, IOException {
    return create(file, keyStore, keyEntity, senderID, UUID.randomUUID().toString(), description);
  }

  /**
   * Create a new JSON message object, sets the various field (type, id, description). It calculates the digest of the submitted
   * file and the filesize and sets them as fields in the message object. A base64 encoded copy of the digest is also included in
   * the sender signature, along with the messageID. The signature is signed by the private key in KeyStore that matches the
   * keyEntity provided. The senderID is the system wide unique ID for the sender and should match the name associated with its
   * certificate. This value will be used by the receiver to check the sender signature. Note: the MBB will only accept file
   * submissions from devices that have their public key in the FileMessage certificate store, otherwise it will be rejected.
   * 
   * @param file
   *          File to send
   * @param keyStore
   *          TVSKeyStore containing a BLS private key for the keyEntity
   * @param keyEntity
   *          String of the name of the entity to retrieve the BLS key for
   * @param senderID
   *          String of the system wide unique ID for this sender
   * @param msgID
   *          String message ID, this must be unique across all submissions
   * @param description
   *          String description of what is being sent - mainly used for logging
   * @return JSONObject of the constructed message
   * @throws NoSuchAlgorithmException
   * @throws TVSKeyStoreException
   * @throws TVSSignatureException
   * @throws JSONException
   * @throws IOException
   */
  public static JSONObject create(File file, TVSKeyStore keyStore, String keyEntity, String senderID, String msgID,
      String description) throws NoSuchAlgorithmException, TVSKeyStoreException, TVSSignatureException, JSONException, IOException {

    MessageDigest md = MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST);
    int fileSize = IOUtils.addFileIntoDigest(file, md);
    String digest = IOUtils.encodeData(EncodingType.BASE64, md.digest());
    JSONObject message = new JSONObject();
    TVSSignature tvsSig = new TVSSignature(keyStore.getBLSPrivateKey(keyEntity));
    tvsSig.update(msgID);
    tvsSig.update(digest);
    message.put(FileMessage.TYPE, MESSAGE_TYPE);
    message.put(FileMessage.ID, msgID);
    message.put(FileMessage.FILESIZE, fileSize);
    message.put(FileMessage.DESCRIPTION, description);
    message.put(FileMessage.DIGEST, digest);
    message.put(FileMessage.SENDER_ID, senderID);
    message.put(FileMessage.SENDER_SIG, tvsSig.signAndEncode(EncodingType.BASE64));
    return message;

  }

}
