/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.fields.messages;

import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants.Vote;

/**
 * Constants used by system clients.
 * 
 * @author Chris Culnane
 * 
 */
public class ClientConstants {

  public class AESKeyFile {

    public static final String AES_KEY = "aeskey";
  }

  public class AuditFile {

    public static final String AUDIT_SERIALS = "auditSerials";
  }

  public class BallotAuditCommitResponse {

    public static final String WBB_SIG = MessageFields.JSONWBBMessage.PEER_SIG;
  }

  public class BallotDB {

    public static final String NEXT_BALLOT = "nextBallot";
  }

  public class BallotGenCommitResponse {

    public static final String PEER_ID       = "peerID";
    public static final String SUBMISSION_ID = MessageFields.BallotFileMessage.ID;
    public static final String BALLOT_FILE   = "ballotFile";
    public static final String FIAT_SHAMIR   = "fiatShamir";
    public static final String WBB_SIG       = MessageFields.JSONWBBMessage.PEER_SIG;
  }

  public class UI {

    public static final String UI_TYPE                 = "type";
    public static final String UI_TYPE_GET_STATUS      = "getStatus";
    public static final String UI_TYPE_GET_TYPE        = "getType";
    public static final String UI_TYPE_GEN_SIGNING_KEY = "genSigningKey";
    public static final String UI_TYPE_GET_CSR         = "getCSR";
    public static final String UI_TYPE_GEN_CRYPTO_KEY  = "genCryptoKey";
    public static final String UI_TYPE_GEN_BALLOTS     = "genBallots";
    public static final String UI_TYPE_IMPORT_CERTS    = "importCerts";
    public static final String UI_RESP_TYPE_STATUS     = "status";
    public static final String UI_RESP_TYPE_TYPE       = "clientType";
    public static final String UI_RESP_TASK_ID         = "taskID";
    public static final String UI_RESP_FINISHED        = "finished";
    public static final String UI_RESP_CSR             = "csr";
    public static final String UI_RESP_SSLCSR          = "sslcsr";
    public static final String UI_RESP_ERROR           = "Error";

    public static final String UI_RESP_MSG             = "msg";
    public static final String UI_REQ_CERT_STRING      = "cert";

    public static final String UI_REQ_SSLCERT_STRING   = "sslcert";
  }

  public class UIAUDITResponse extends UIResponse {

    public static final String RACE_ID          = "id";
    //public static final String DISTRICT_RACE    = "LA";
    //public static final String REGION_RACE_ATL  = "LC_ATL";
    //public static final String REGION_RACE_BTL  = "LC_BTL";
    public static final String RACE_PERMUTATION = "permutation";
    public static final String DISTRICT         = "district";
    public static final String RACES            = "races";
  }

  public class UIERROR extends UIResponse {

    public static final String ERROR         = "ERROR";
    public static final String WBB_RESPONSES = "WBB_Responses";
  }

  public class UIMessage {

    public static final String SERIAL_NO = "serialNo";
    public static final String TYPE      = "type";
  }

  public class UIPODMessage extends UIMessage {

    public static final String DISTRICT = "district";
  }

  public class UIPODResponse extends UIResponse {

    public static final String RACE_ID          = "id";
    //public static final String DISTRICT_RACE    = "LA";
    //public static final String REGION_RACE_ATL  = "LC_ATL";
    //public static final String REGION_RACE_BTL  = "LC_BTL";
    public static final String RACE_PERMUTATION = "permutation";
    public static final String DISTRICT         = "district";
    public static final String RACES            = "races";
  }

  public class UIResponse {

    public static final String WBB_SIGNATURES = "sigs";
  }

  public class UISTARTEVMMessage extends UIMessage {

    public static final String DISTRICT = "district";
  }

  public class UISTARTEVMResponse extends UIResponse {

    public static final String COMMIT_TIME = "commitTime";

  }

  public class UIVOTEMessage extends UIMessage {

    public static final String DISTRICT             = "district";
    public static final String RACES                = Vote.RACES;
    public static final String RACE_ID              = Vote.RACE_ID;
    //public static final String DISTRICT_RACE        = Vote.DISTRICT_RACE;
    //public static final String REGION_RACE_ATL      = Vote.REGION_RACE_ATL;
    //public static final String REGION_RACE_BTL      = Vote.REGION_RACE_BTL;
    public static final String PREFERENCES          = Vote.PREFERENCES;
    public static final String RACE_SEPARATOR       = MessageFields.RACE_SEPARATOR;
    public static final String PREFERENCE_SEPARATOR = MessageFields.PREFERENCE_SEPARATOR;
  }
  public class UICANCELAUTHMessage extends UIMessage {

    public static final String DISTRICT             = "district";
    
  }
  public class UIVOTEResponse extends UIResponse {

    public static final String COMMIT_TIME = "commitTime";

  }

  public class X509 {

    public static final String COMMON_NAME       = "CN";
    public static final String ORGANISATION      = "O";
    public static final String ORGANISATION_UNIT = "OU";
    public static final String COUNTRY           = "C";
    public static final String LOCATION          = "L";
    public static final String STATE             = "ST";
  }

  public static final String DEFAULT_MESSAGE_DIGEST = SystemConstants.DEFAULT_MESSAGE_DIGEST;
  public static final String REQUEST_MESSAGE_PARAM  = "msg";
  public static String       CERT_FILE_EXT          = ".pem";

  public static String       SSLCERT_FILE_SUFFIX    = "SSL";
}
