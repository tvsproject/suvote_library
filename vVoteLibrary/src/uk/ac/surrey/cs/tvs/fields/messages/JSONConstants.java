/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.fields.messages;

/**
 * Constants used in JSON messages.
 * 
 * @author Chris Culnane
 * 
 */
public class JSONConstants {

  /**
   * Progress constants.
   */
  public class ProgressMessage {

    public static final String TYPE                 = "type";
    public static final String PROGRESS_ID          = "progressID";
    public static final String VALUE                = "value";
    public static final String TYPE_PROGRESS_UPDATE = "progressUpdate";
  }

  /**
   * Schema constants.
   */
  public class SchemaProcessing {

    public static final String SCHEMA_JS       = "tv4.min.js";
    public static final String DATA_VARIABLE   = "data";
    public static final String SCHEMA_VARIABLE = "schema";
    public static final String IS_VALID        = "valid";
  }

  /**
   * Signature constants.
   */
  public class Signature {

    public static final String SIGNER_ID = "WBBID";
    public static final String SIGNATURE = "WBBSig";
  }

  /**
   * Vote constants.
   */
  public class Vote {

    //public static final String DISTRICT_RACE        = "LA";
    //public static final String REGION_RACE_ATL      = "LC_ATL";
    //public static final String REGION_RACE_BTL      = "LC_BTL";
    public static final String PREFERENCES          = "preferences";
    public static final String RACES                = "races";
    public static final String RACE_ID              = "id";
    public static final String RACE_SEPARATOR       = MessageFields.RACE_SEPARATOR;
    public static final String PREFERENCE_SEPARATOR = MessageFields.PREFERENCE_SEPARATOR;
  }
}
