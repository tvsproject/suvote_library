/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.padding;

import org.json.JSONException;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Utility to generate a random padding point
 * 
 * @author Chris Culnane
 * 
 */
public class GeneratePaddingPoint {

  /**
   * Default constructor
   */
  public GeneratePaddingPoint() {
  }

  /**
   * Generates a padding point at random and store it in the filename specified
   * 
   * @param filename
   *          String file path to store generated padding point
   * 
   * @throws JSONIOException
   * @throws JSONException
   */
  public static void generatePaddingPoint(String filename) throws JSONIOException, JSONException {
    ECUtils ecUtils = new ECUtils();
    IOUtils.writeJSONToFile(ECUtils.ecPointToJSON(ecUtils.getRandomValue()), filename);
    // TODO Verify Padding Point does not exist in candidate IDs
  }

  /**
   * main method to generate the padding point and store in the file specified in args
   * 
   * @param args
   *          String array of args containing path to store padding point
   * @throws JSONIOException
   * @throws JSONException
   */
  public static void main(String[] args) throws JSONIOException, JSONException {
    if (args.length == 0) {
      System.out.println("Generating Padding Point and Saving to paddingpoint.json");
      GeneratePaddingPoint.generatePaddingPoint("paddingpoint.json");
    }
    else if (args.length == 1) {
      System.out.println("Generating Padding Point and Saving to " + args[0]);
      GeneratePaddingPoint.generatePaddingPoint(args[0]);
    }
    else {
      System.out.println("Usage: GeneratePaddingPoint [path]");
      System.out.println("\tpath is an option file path to save the padding point to");
      System.out.println("\totherwise it saves to paddingpoint.json");
    }

  }

}
