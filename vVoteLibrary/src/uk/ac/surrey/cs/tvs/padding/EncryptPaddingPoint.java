/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.padding;

import java.math.BigInteger;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Utility to encrypt the padding point to be used in preparation of data for mixing
 * 
 * @author Chris Culnane
 * 
 */
public class EncryptPaddingPoint {

  /**
   * Default constructor
   */
  public EncryptPaddingPoint() {
  }

  /**
   * Encrypts the padding point contained within the paddingPoint file using the public key in publicKeyPath and stores the result
   * to the path in output
   * 
   * @param paddingPoint
   *          String path to JSONObject file with the padding point
   * @param output
   *          String path to write the encrypted padding point to
   * @param publicKeyPath
   *          String path the JSON public key
   * @throws JSONIOException
   * @throws JSONException
   */
  public static void encryptPaddingPoint(String paddingPoint, String output, String publicKeyPath) throws JSONIOException,
      JSONException {
    ECUtils ecUtils = new ECUtils();
    ECPoint padPoint = ecUtils.pointFromJSON(IOUtils.readJSONObjectFromFile(paddingPoint));
    ECPoint publicKey = ecUtils.pointFromJSON(IOUtils.readJSONObjectFromFile(publicKeyPath));
    ECPoint[] encryptedPadPoint = ecUtils.encrypt(padPoint, publicKey, BigInteger.ONE);
    IOUtils.writeJSONToFile(ecUtils.cipherToJSON(encryptedPadPoint), output);
    // TODO Verify Padding Point does not exist in candidate IDs
  }

  /**
   * Main method for calling this utility, usage instructions given below
   * 
   * @param args
   *          String of args containing paths to various files, see usage instructions
   * @throws JSONIOException
   * @throws JSONException
   */
  public static void main(String[] args) throws JSONIOException, JSONException {
    if (args.length == 3) {
      System.out.println("Encrypting Padding point " + args[0] + " using " + args[1] + ", saving to " + args[2]);
      EncryptPaddingPoint.encryptPaddingPoint(args[0], args[2], args[1]);
    }
    else {
      System.out.println("Usage: EncryptPaddingPoint paddigPointPath publicKeyPath outputPath");
      System.out.println("\tpaddingPointPath - path to JSON padding point");
      System.out.println("\tpublicKeyPath - path to JSON Public Key");
      System.out.println("\toutputPath - path to save encrypted point to");
    }

  }

}
