/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import java.security.cert.Certificate;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPublicKey;

/**
 * TVSCertificate provides a wrapper around standard java.security.cert.Certificates and BLSPublicKeys. It is used in combination
 * with the TVSKeyStore to provide a more seamless integration of the BLS Signatures with the existing Signature schemes. Due to
 * extending restrictions on the java security classes it is not possible to extend the classes as one normally would, to provide
 * the additional functionality required by BLS. We are left with the alternative of wrapping both objects and getting the
 * appropriate type of object when performing the actual operation. This increases the responsibility on the programmer to make sure
 * they are constructing and using the certificate correctly, otherwise they will cause exceptions by passing invalid objects. For
 * example, getting a TVSCertificate containing a BLS signature and then passing it to a TVSSignature initialised for ECDSA. The
 * objects themselves are valid, but the underlying contents would not be.
 * 
 * Currently BLS keys do not have actual certificates because the BLS algorithm is not recognised by any CAs or by KeyStores, hence
 * they need to be stored and processed outside of the normal signature structures.
 * 
 * @author Chris Culnane
 * 
 */
public class TVSCertificate {

  /**
   * Certificate object, only set when accessing a standard java.security.cert.Certificate
   */
  private Certificate  cert;
  /**
   * BLSPublicKey object, only set when accessing a BLSKey
   */
  private BLSPublicKey blsPubKey;


  /**
   * Constructs a TVSCertificate from a standard Java Certificate
   * @param cert Certificate to be wrapped by a TVSCertificate
   */
  public TVSCertificate(Certificate cert) {
    this.cert = cert;
  }

  /**
   * Constructs a TVSCertificate from a BLSPublicKey - this could be a threshold or standard BLSPublicKey
   * @param pubKey BLSPublicKey object to be wrapped by a TVSCertificate
   */
  public TVSCertificate(BLSPublicKey pubKey) {
    this.blsPubKey = pubKey;
  }

  /**
   * Gets the underlying standard Java certificate or null if it hasn't been set
   * @return Certificate of the underlying certificate
   */
  public Certificate getCertificate() {
    return this.cert;
  }

  /**
   * Gets the underlying BLSPublicKey object if it has been set or null if it hasn't been set
   * @return BLSPublicKey object
   */
  public BLSPublicKey getBLSPublicKey() {
    return this.blsPubKey;
  }

}
