/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.conf;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * RaceDefs contains all of the defined RaceDef items, indexed by order and ID to allow quick access
 * 
 * @author Chris Culnane
 * 
 */
public class RaceDefs {

  /**
   * Indexes the RaceDef objects by Id
   */
  private volatile HashMap<String, RaceDef> raceDefs = new HashMap<String, RaceDef>();

  /**
   * Index of RaceDef ids by the order they were defined
   */
  private volatile ArrayList<String>        raceIds  = new ArrayList<String>();

  /**
   * Default empty constructor
   */
  public RaceDefs() {
  }

  /**
   * Adds a RaceDef to the list of RaceDefs
   * 
   * @param raceDef
   *          RaceDef to add
   */
  public synchronized void addRaceDef(RaceDef raceDef) {
    this.raceDefs.put(raceDef.getID(), raceDef);
    this.raceIds.add(raceDef.getID());
  }

  /**
   * Gets an array list of RaceIds indexed by the order they were added
   * 
   * @return ArrayList of string raceIds
   */
  public ArrayList<String> getRaceIds() {
    return this.raceIds;
  }

  /**
   * Gets an individual RaceDef by id
   * 
   * @param id
   *          String id of RaceDef to retrieve
   * @return RaceDef matching the id or null if it does not exist
   */
  public RaceDef getRaceDef(String id) {
    return this.raceDefs.get(id);
  }
}
