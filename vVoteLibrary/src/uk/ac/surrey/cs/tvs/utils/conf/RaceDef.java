/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.conf;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Defines a single race in an election and the restrictions that are applied to it. It contains a minimum of three keys, with an
 * optional three further values. The following keys are required:
 * <ul>
 * <li>id - the id of the race, case-sensitive</li>
 * <li>candidates - the maximum number of candidates in the race</li>
 * <li>type - Ranked or Single to indicate the type of race it is</li>
 * <li>excludes - [optional] JSONArray of race ids that cannot be voted in if this race is voted in. i.e. ATL vs BTL</li>
 * <li>minNumPrefs - [optional] the minimum number of preferences that must be submitted</li>
 * <li>maxNumPrefs - [optional] the maximum number of preferences that may be subbmitted</li>
 * </ul>
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class RaceDef {

  /**
   * RaceDef ID key
   */
  public static final String RACEDEF_ID          = "id";
  /**
   * RaceDef number of candidates key
   */
  public static final String RACEDEF_CANDIDATES  = "candidates";

  /**
   * RaceDef type of race key
   */
  public static final String RACEDEF_TYPE        = "type";

  /**
   * RaceDef minimum number of preferences key
   */
  public static final String RACEDEF_MINPREFS    = "minNumPrefs";

  /**
   * RaceDef maximum number of preferences key
   */
  public static final String RACEDEF_MAXPREFS    = "maxNumPrefs";
  /**
   * RaceDef excludes if voted in key
   */
  public static final String RACEDEF_EXCLUDES    = "excludes";

  /**
   * Single race type
   */
  public static final String RACEDEF_TYPE_SINGLE = "Single";
  /**
   * Ranked race type
   */
  public static final String RACEDEF_TYPE_RANKED = "Ranked";

  /**
   * String that holds the RaceDef id
   */
  private String             id;

  /**
   * String that holds the RaceDef type
   */
  private RaceType           type;
  /**
   * int that stores the number of candidates
   */
  private int                candidates;

  /**
   * int that stores the minimum number of preferences that must be submitted
   */
  private int                minPrefs;
  /**
   * int that stores the maximum number of preferences that may be submitted
   */
  private int                maxPrefs;
  /**
   * JSONArray of races to be excluded if this race is voted in
   */
  private JSONArray          excludes            = new JSONArray();

  /**
   * Constructs a new RaceDef object from a JSONObject that contains the minimum RaceDef specification
   * 
   * @throws JSONException
   * 
   */
  public RaceDef(JSONObject raceDef) throws JSONException {
    this.id = raceDef.getString(RACEDEF_ID);
    this.type = RaceType.valueOf(raceDef.getString(RACEDEF_TYPE).toUpperCase());
    this.candidates = raceDef.getInt(RACEDEF_CANDIDATES);
    this.minPrefs = raceDef.optInt(RACEDEF_MINPREFS, -1);
    this.maxPrefs = raceDef.optInt(RACEDEF_MAXPREFS, -1);
    JSONArray excludes = raceDef.optJSONArray(RACEDEF_EXCLUDES);
    if (excludes != null) {
      this.excludes = excludes;
    }
  }

  /**
   * Gets the RaceDef id
   * 
   * @return String containing race id
   */
  public String getID() {
    return this.id;
  }

  /**
   * Gets the RaceDef type
   * 
   * @return String containing type of race, either Single or Ranked
   */
  public RaceType getType() {
    return this.type;
  }

  /**
   * Gets the number of candidates in the this race
   * 
   * @return int of the number of candidates
   */
  public int getCandidateCount() {
    return this.candidates;
  }

  /**
   * Gets the JSONArray containing the list of excluded races if this race is voted in
   * 
   * @return JSONArray containing strings of RaceDef ids to be excluded
   */
  public JSONArray getExcludes() {
    return this.excludes;
  }

  /**
   * Checks if the RaceDef specified a minimum number of preferences
   * 
   * @return boolean true if set, false if not
   */
  public boolean hasMinSet() {
    if (minPrefs >= 0) {
      return true;
    }
    return false;
  }

  /**
   * Gets the minimum number of preferences that must be submitted
   * 
   * @return int containing minimum number of preferences required
   */
  public int getMinPrefs() {
    return this.minPrefs;
  }

  /**
   * Checks if the maximum number of preferences was set in the RaceDef
   * 
   * @return boolean true if it was set, false if not
   */
  public boolean hasMaxSet() {
    if (maxPrefs >= 0) {
      return true;
    }
    return false;
  }

  /**
   * Gets the maximum allowed preferences for this race
   * 
   * @return int of the maximum allowed preferences
   */
  public int getMaxPrefs() {
    return this.maxPrefs;
  }

}
