/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.verify;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.conf.RaceDef;
import uk.ac.surrey.cs.tvs.utils.conf.RaceDefs;

/**
 * Utility class for verifying preference lists are valid. This should be used by the Servlet and the MBB to check that the incoming
 * preferences are valid and therefore should be accepted/sent.
 * 
 * @author Chris Culnane
 * 
 */
public class VerifyPreferences {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(VerifyPreferences.class);

  /**
   * Checks LA, LC-ATl and LC-BTL to ensure collectively they form a valid ballot. The LA vote is checked independently, whilst the
   * ATL and BTL votes are checked for consistency. For example, you cannot vote in both. The actual checking if performed by other
   * methods, this merely builds the JSONArrays and checkes the results to determine the overall result.
   * 
   * @param races
   *          JSONArray of races and preferences that conforms the schema standard
   * @return PrefsVerification of the result
   * @throws JSONException
   */
  public static PrefsVerification checkAllRacePreferences(JSONArray races, RaceDefs raceDefs) throws JSONException {
    logger.info("Checking all preferences of {}", races.toString());

    HashMap<String, PrefsVerification> verificationResults = new HashMap<String, PrefsVerification>();
    ArrayList<String> raceIds = raceDefs.getRaceIds();
    for (int i = 0; i < races.length(); i++) {
      JSONObject race = races.getJSONObject(i);
      RaceDef raceDef = raceDefs.getRaceDef(race.getString(MessageFields.VoteMessage.RACE_ID));
      if (raceDef == null) {
        logger.info("No race definition found, cannot check the preferences");
        return PrefsVerification.INVALID;
      }
      // Check the race ids are unique
      if (verificationResults.containsKey(race.getString(MessageFields.VoteMessage.RACE_ID))) {
        logger.info("Duplicate race submitted, invalid");
        return PrefsVerification.INVALID;
      }
      PrefsVerification verificationResult;
      switch (raceDef.getType()) {
        case SINGLE:
          verificationResult = checkSinglePref(race.getJSONArray(MessageFields.VoteMessage.PREFERENCES));
          if (verificationResult != PrefsVerification.SINGLE_PREF && verificationResult != PrefsVerification.EMPTY) {
            logger.info("Preferences are invalid will stop checking the remainder");
            return PrefsVerification.INVALID;
          }
          verificationResults.put(race.getString(MessageFields.VoteMessage.RACE_ID), verificationResult);
          break;
        case RANKED:
          verificationResult = checkPrefs(race.getJSONArray(MessageFields.VoteMessage.PREFERENCES));
          if (verificationResult != PrefsVerification.VALID_PREF && verificationResult != PrefsVerification.EMPTY) {
            logger.info("Preferences are invalid will stop checking the remainder");
            return PrefsVerification.INVALID;
          }
          verificationResults.put(race.getString(MessageFields.VoteMessage.RACE_ID), verificationResult);
          break;
        default:
          logger.info("Unknown race type, cannot check the preferences");
          return PrefsVerification.INVALID;
      }
    }
    for (String raceId : raceIds) {
      RaceDef raceDef = raceDefs.getRaceDef(raceId);
      PrefsVerification verificationResult = verificationResults.get(raceId);
      if (verificationResult == null) {
        logger.info("Missing race submission");
        return PrefsVerification.INVALID;
      }
      if (verificationResult != PrefsVerification.EMPTY && verificationResult != PrefsVerification.INVALID) {
        // We have a valid entry in this race, therefore excluded races should be empty
        JSONArray excludes = raceDef.getExcludes();
        for (int i = 0; i < excludes.length(); i++) {
          PrefsVerification excludeVeri = verificationResults.get(excludes.getString(i));
          if (excludeVeri == null || excludeVeri != PrefsVerification.EMPTY) {
            logger.info("Inconsistency with exclude clause on preferences. Invaldd");
            return PrefsVerification.INVALID;
          }
        }
      }
    }
    return PrefsVerification.ALL_VALID;

  }

  /**
   * Checks a set of preferences in a JSONArray to ensure they are valid. Valid indicates the set of preferences is a set of numbers
   * that range from 1 to n sequentially, with no repeats.
   * 
   * @param prefs
   *          JSONArray of preferences where preferences are strings, with empty preferences being a single space character
   * @return PrefsVerification result (INVALID, EMPTY or VALID_PREF)
   * @throws JSONException
   */
  public static PrefsVerification checkPrefs(JSONArray prefs) throws JSONException {
    try {
      if (prefs == null) {
        logger.warn("Prefs array was null");
        return PrefsVerification.INVALID;
      }

      int[] prefCheck = new int[prefs.length() + 1];
      int maxPrefFound = 0;

      for (int i = 0; i < prefs.length(); i++) {
        String prefString = prefs.getString(i);

        if (!prefString.equals(" ")) {
          int prefInt = Integer.parseInt(prefString);

          if (prefInt > prefs.length()) {
            logger.warn("Preference is larger than the maximum preference allowed");
            return PrefsVerification.INVALID;
          }

          prefCheck[prefInt]++;
          if (prefInt > maxPrefFound) {
            maxPrefFound = prefInt;
          }
        }
      }

      if (prefCheck[0] != 0) {
        logger.warn("Preference of zero was received");
        return PrefsVerification.INVALID;
      }

      boolean foundZero = false;

      for (int i = 1; i < prefCheck.length; i++) {
        if (prefCheck[i] == 0) {
          foundZero = true;
        }
        else if (prefCheck[i] > 1) {
          logger.warn("The preference {} appeared {} times - this is invalid", i, prefCheck[i]);
          return PrefsVerification.INVALID;
        }
        else if (foundZero == true && prefCheck[i] == 1) {
          logger.warn("Preferences are not a sequential list of numbers");
          return PrefsVerification.INVALID;
        }
      }

      if (maxPrefFound == 0) {
        return PrefsVerification.EMPTY;
      }
      else {
        return PrefsVerification.VALID_PREF;
      }
    }
    catch (Exception e) {
      logger.warn("An exception occured when processing the preference list - possible non-numeric character", e);
      return PrefsVerification.INVALID;
    }
  }

  /**
   * Checks that a JSONArray of preferences contains a single 1 and the rest are string consisting of a single space character
   * 
   * @param prefs
   *          JSONArray of preferences where preferences are strings, with empty preferences being a single space character
   * @return PrefsVerification result (INVALID, EMPTY or SINGLE_PREF)
   * @throws JSONException
   */
  public static PrefsVerification checkSinglePref(JSONArray prefs) throws JSONException {
    if (prefs == null) {
      logger.warn("Prefs array was null");
      return PrefsVerification.INVALID;
    }

    int prefCount = 0;

    for (int i = 0; i < prefs.length(); i++) {
      if (prefs.getString(i).equals(" ")) {
        // do nothing we have this clause for empty arrays to optimise checking
      }
      else if (prefs.getString(i).equals("1")) {
        prefCount++;
      }
      else {
        logger.warn("Preference value was something other than a space of 1: {}", prefs.getString(i));
        return PrefsVerification.INVALID;
      }
    }

    if (prefCount == 0) {
      return PrefsVerification.EMPTY;
    }
    else if (prefCount == 1) {
      return PrefsVerification.SINGLE_PREF;
    }
    else {
      logger.warn("Prefcount is {}, this is invalid", prefCount);
      return PrefsVerification.INVALID;
    }
  }
}
