/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Pairing;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSCertificate;

/**
 * BLSSignature class for constructing a verifying a signature. This class is modelled on the standard Signature class. Ideally,
 * this would be an extension of the Signature class, however, due to Java restrictions it is not possible to extend Signature in
 * the normal way. As such, we have mirrored the functionality to make it as intuitive as possible to use. The verify method has
 * been expanded to handle both normal verification and verification of a partial signature. The underlying signature algorithm is
 * simple enough that the actual processing is identical, we just use a partial public key in the threshold setting.
 * 
 * The actual operation implemented here follows both http://crypto.stanford.edu/pbc/manual/ch02s01.html and the Java port
 * http://gas.dia.unisa.it/projects/jpbc/schemes/bls.html
 * 
 * @author Chris Culnane
 * 
 */
public class BLSSignature {

  /**
   * Underlying message digest that we use for creating the hash
   */
  private MessageDigest md;

  /**
   * Reference to BLSPrivateKey - will only be set if we are creating a signature
   */
  private BLSPrivateKey privateKey;

  /**
   * Reference to BLSPublicKey - will only be set if we are verifying a signature
   */
  private BLSPublicKey  publicKey;

  /**
   * Gets an instance of the BLSSignature that uses the underlying MessageDigest specified in messageDigest. The String
   * messageDigest must be one of the available MessageDigest instances available on the system, otherwise an exception will be
   * thrown.
   * 
   * @param messageDigest
   *          String recognised MessageDigest (SHA1, SHA256)
   * @return BLSSignature instance initialised with the specified message digest
   * @throws NoSuchAlgorithmException
   */
  public static BLSSignature getInstance(String messageDigest) throws NoSuchAlgorithmException {
    return new BLSSignature(messageDigest);
  }

  /**
   * Private constructor for use in the getInstance method to construct the actual instance of the BLSSignature
   * 
   * @param messageDigest
   *          String message digest
   * @throws NoSuchAlgorithmException
   */
  private BLSSignature(String messageDigest) throws NoSuchAlgorithmException {
    md = MessageDigest.getInstance(messageDigest);
  }

  /**
   * Initialises the BLSSignature object for signing by setting the BLSPrivateKey to use.
   * 
   * @param privKey
   *          BLSPrivateKey to use for signing
   */
  public void initSign(BLSPrivateKey privKey) {
    md.reset();
    this.privateKey = privKey;
  }

  /**
   * Initialises the BLSSignature object for verification by setting the BLSPublicKey to use. This will also reset the message
   * digest. This is a small optimisation that allows a BLSSignature object to be reused for verifying many messages.
   * 
   * @param pubKey
   *          BLSPublicKey to use for verification
   */
  public void initVerify(BLSPublicKey pubKey) {
    this.publicKey = pubKey;
    md.reset();

  }

  /**
   * Initialises the BLSSignature object for verification by setting the TVSCertificate to use. The TVSCertificate must contain a
   * BLSPublicKey, otherwise an exception will be thrown.
   * 
   * This will also reset the message digest. This is a small optimisation that allows a BLSSignature object to be reused for
   * verifying many messages.
   * 
   * @param cert
   *          TVSCertificate to get BLSPublicKey from for use during verification
   */
  public void initVerify(TVSCertificate cert) throws TVSSignatureException {
    if (cert.getBLSPublicKey() == null) {
      throw new TVSSignatureException("Null BLS Public Key - probably wrong certificate");
    }
    initVerify(cert.getBLSPublicKey());

  }

  /**
   * Update the underlying message digest with the string contents passed in. This is a utility method that will call getBytes() on
   * the string to add the bytes to the underlying message digest. If operating across different platforms, or string encoding is an
   * issue, it is better to manually construct the bytes of the string and then call update(byte[] data).
   * 
   * @param data
   *          String to add to the message digest
   */
  public void update(String data) {
    this.md.update(data.getBytes());
  }

  /**
   * Update the underlying message digest with the contents of the byte array data
   * 
   * @param data
   *          byte[] of data to add to the message digest
   */
  public void update(byte[] data) {
    this.md.update(data);
  }

  /**
   * Signs the contents of the underlying message digest using the BLSPrivateKey. If no BLSPrivateKey has been set, i.e. initSign
   * was not called previously, an exception will be thrown.
   * 
   * The actual signing algorithm is as follows:
   * <ul>
   * <li>Take the bytes from the MessageDigest and map them onto an element (h) on G1</li>
   * <li>Compute h^x (where x = privateKey) to calculate the signature</li>
   * </ul>
   * 
   * @return Element containing the signature
   * @throws TVSSignatureException
   */
  public Element sign() throws TVSSignatureException {
    if (this.privateKey == null) {
      // if the privatekey is null we cannot generate the signature
      throw new TVSSignatureException("Incorrectly initialised BLSSignature object, cannot sign without a non-null BLSPrivateKey");
    }
    // Get the hash bytes
    byte[] hash = md.digest();

    // Map them onto an element in G1
    Element h = CurveParams.getInstance().getPairing().getG1().newElement().setFromHash(hash, 0, hash.length).getImmutable();

    // Calculate h^x, where x is the private key
    return h.powZn(this.privateKey.getKey());
  }

  /**
   * Verifies the contents of the message digest with the specified signature bytes. This first converts the bytes into an Element
   * and then verifies it as a standard signature. This defaults the partial parameter to false. This method calls the verify(Element
   * sig, boolean partial) method to perform the actual verification. See @see {@link BLSSignature#verify(Element, boolean)} for
   * full details.
   * 
   * @param signature
   *          byte array of signature bytes
   * @return boolean true if the signature is valid, false if invalid
   * @throws TVSSignatureException
   */
  public boolean verify(byte[] signature) throws TVSSignatureException {
    return verify(signature, false);
  }

  /**
   * Verifies the contents of the message digest with the specified signature bytes. This first converts the bytes into an Element
   * and then verifies it as a standard signature. This method calls the verify(Element sig, boolean partial) method to perform the
   * actual verification. See @see {@link BLSSignature#verify(Element, boolean)} for full details.
   * 
   * @param signature
   *          byte array of signature bytes
   * @param partial
   *          boolean true if verifying a threshold signature share, false if verifying a standard signature or joint signature
   * @return boolean true if the signature is valid, false if invalid
   * @throws TVSSignatureException
   */
  public boolean verify(byte[] signature, boolean partial) throws TVSSignatureException {
    return verify(getSignatureElement(signature), partial);

  }

  /**
   * Utility method for converting a byte array containing the byte contents of the signature Element, back into an Element of G1.
   * This is useful when saving and loading signatures from files, where they are likely to have been stored as pure bytes.
   * 
   * @param signature byte array containing a signature element
   * @return Element on G1 of the byte array
   */
  public static Element getSignatureElement(byte[] signature) {
    Element sig = CurveParams.getInstance().getPairing().getG1().newElement();
    sig.setFromBytes(signature);
    return sig;
  }

  /**
   * Performs the actual signature verification using the contents of the message digest that holds the data submitted via the
   * update methods. If no PublicKey has been set an exception will be thrown. This method can handle both normal BLS and threhsold
   * BLS. If the partial parameter is set to true the verification will get the BLSPartialPublicKey instead of the full public key
   * from the specified BLSPublicKey object.
   * 
   * The actual verification is as follows:
   * <ul>
   * <li>Take the bytes from the MessageDigest and map them onto an element (h) on G1</li>
   * <li>Create a pairing between the signature element and the generator g</li>
   * <li>Create a second pairing between the hash element h and the public key element</li>
   * <li>Check the pairings are equal, if they are the signature is valid, else it is invalid</li>
   * </ul>
   * Note: the generator g is a system parameter, it is out of convenience we choose to store it in the public key object.
   * 
   * The actual verification is checking that e(signature, g) = e(h, g^x), where is e represents a pairing.
   * 
   * @param signature
   *          Element containing the signature
   * @param partial
   *          boolean set to true if verifying a partial signature, false if a standard signature
   * @return boolean true if valid, false if invalid
   * @throws TVSSignatureException
   */
  public boolean verify(Element signature, boolean partial) throws TVSSignatureException {
    if (this.publicKey == null) {
      throw new TVSSignatureException("Incorrectly initialised BLSSignature object, cannot verify without a non-null BLSPublicKey");
    }
    // get either the full public key or the partial public key
    Element pubKey = null;
    if (partial) {
      pubKey = this.publicKey.getPartialPublicKey();
    }
    else {
      pubKey = this.publicKey.getPublicKey();
    }

    // Check we actually have a valid key
    if (pubKey == null) {
      throw new TVSSignatureException(
          "Incorrectly initialised BLSSignature object, the specified BLSPublicKey does not contain a valid key.");
    }

    // Gets a reference to the curve pairing object
    Pairing pairing = CurveParams.getInstance().getPairing();

    // Construct the hash bytes
    byte[] hash = md.digest();

    // Map the hash onto an element in G1
    Element h = CurveParams.getInstance().getPairing().getG1().newElement().setFromHash(hash, 0, hash.length).getImmutable();

    // Create the signature pairing
    Element sigPairing = pairing.pairing(signature, this.publicKey.getG());

    // Create the hash pairing
    Element hashPairing = pairing.pairing(h, pubKey);

    // If the pairing are equal the signature is valid
    return sigPairing.isEqual(hashPairing);

  }
}
