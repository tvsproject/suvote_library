/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto;

import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.crypto.Commitment;
import org.bouncycastle.crypto.commitments.HashCommitter;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.prng.FixedSecureRandom;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CommitException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Utility class containing various Crypto Utility method. These range from key generation to IO operations like loading and saving
 * keys pair to files.
 * 
 * @author Chris Culnane
 * 
 */
public class CryptoUtils {

  /**
   * Static variable to track if we have added the BC provider
   */
  private static boolean                   providerAdded;

  /**
   * Default algorithm string to use for AES
   */
  private static final String              DEFAULT_AES_ALGORITHM = "AES/ECB/NoPadding";

  /**
   * Logger
   */
  private static final Logger              logger                = LoggerFactory.getLogger(CryptoUtils.class);

  /**
   * Explicit reference to BouncyCastleProvider we can use instead of "BC". This will then work on Android after JarJar renaming
   */
  public static final BouncyCastleProvider BC_PROV               = new BouncyCastleProvider();

  /**
   * Checks a commitment against a claimed witness and data value. Calls the underlying BouncyCastle commitment scheme to perform
   * the check. If the data is longer than half the length of the underlying hash (SHA256, which has a block size of 64 bytes) it
   * will first be hashed using SHA256 to produce a 32 byte value. This matches the operations performed during commitment
   * generation.
   * 
   * @param commitment
   *          byte array containing commitment value
   * @param witness
   *          byte array of claimed witness value
   * @param data
   *          byte array of claimed data value
   * @return boolean true if the opening is valid, false if not
   * @throws CommitException
   */
  public static boolean checkCommitment(byte[] commitment, byte[] witness, byte[] data) throws CommitException {
    try {
      HashCommitter hashCommit = new HashCommitter(new SHA256Digest(), new FixedSecureRandom(witness));
      if (data.length > 32) {
        MessageDigest md = MessageDigest.getInstance("SHA256");
        data = md.digest(data);
      }
      Commitment com = new Commitment(witness, commitment);

      return hashCommit.isRevealed(com, data);
    }
    catch (NoSuchAlgorithmException e) {
      throw new CommitException(e);
    }
  }

  /**
   * Performs an ElGamal decryption of the cipherText using the PrivateKey passed in
   * 
   * @param cipherText
   *          byte array of cipherText
   * @param privKey
   *          PrivateKey to use in decryption
   * @return byte array of plaintext
   * @throws CryptoIOException
   */
  public static byte[] elGamalDecrypt(byte[] cipherText, PrivateKey privKey) throws CryptoIOException {
    try {
      Cipher cipher = Cipher.getInstance("ElGamal/None/NoPadding", CryptoUtils.BC_PROV);
      cipher.init(Cipher.DECRYPT_MODE, privKey);

      return cipher.doFinal(cipherText);
    }
    catch (NoSuchAlgorithmException e) {
      throw new CryptoIOException("Exception whilst decryption AESKey", e);
    }
    catch (NoSuchPaddingException e) {
      throw new CryptoIOException("Exception whilst decryption AESKey", e);
    }
    catch (InvalidKeyException e) {
      throw new CryptoIOException("Exception whilst decryption AESKey", e);
    }
    catch (IllegalBlockSizeException e) {
      throw new CryptoIOException("Exception whilst decryption AESKey", e);
    }
    catch (BadPaddingException e) {
      throw new CryptoIOException("Exception whilst decryption AESKey", e);
    }
  }

  /**
   * Utility method for generating a commitment from the data and fixed witness (random data) passed in. Uses the underlying
   * BouncyCastle commitment scheme.
   * 
   * This is designed to be used when the data and witness are the same length and exactly half the block size of the underlying
   * hash - which in this case is SHA256 with a block size of 64 bytes. If the data passed in is greater than half the underlying
   * hash it will be hashed first using SHA256 to produce a 32 byte output.
   * 
   * TODO - we need to handle shorted messages as well - otherwise an ArrayIndexOutOfBounds exception will be thrown - possibly Hash
   * short messages as well
   * 
   * @param data
   *          byte array of data which is at least half the length of the underlying block (block is 64 bytes, this should be 32
   *          bytes)
   * @param randomData
   *          byte array that is exactly half the length of the underlying block (block is 64 bytes, this should be 32 bytes)
   * @return byte array of commitment value
   * @throws CommitException
   */
  public static byte[] generateCommitment(byte[] data, byte[] randomData) throws CommitException {
    try {
      byte[][] commData = new byte[2][];

      if (data.length > 32) {
        MessageDigest md = MessageDigest.getInstance("SHA256");
        data = md.digest(data);
      }

      HashCommitter hashCommit = new HashCommitter(new SHA256Digest(), new FixedSecureRandom(randomData));
      Commitment com = hashCommit.commit(data);
      commData[0] = com.getCommitment();
      commData[1] = com.getSecret();

      return commData[0];
    }
    catch (NoSuchAlgorithmException e) {
      throw new CommitException(e);
    }
  }

  /**
   * Utility method for generating a commitment from the data, using a SecureRandom source to generate a suitable witness (random)
   * value. Uses the underlying BouncyCastle Commitment scheme. If the data passed in is greater than half the underlying block size
   * (SHA256, block size of 64 bytes) it will first be hashed with SHA256 to produce a 32 byte value to commit to. TODO - we need to
   * handle shorted messages as well - otherwise an ArrayIndexOutOfBounds exception will be thrown - possibly Hash short messages as
   * well
   * 
   * @param data
   *          byte array of data to commit to
   * @param randomSource
   *          SecureRandom to use to generate suitable witness value from
   * @return array of byte arrays containing commitment value and secret value [commitment,witness]
   * @throws CommitException
   */
  public static byte[][] generateCommitment(byte[] data, SecureRandom randomSource) throws CommitException {
    try {
      byte[][] commData = new byte[2][];

      if (data.length > 32) {
        MessageDigest md = MessageDigest.getInstance("SHA256");
        data = md.digest(data);
      }

      HashCommitter hashCommit = new HashCommitter(new SHA256Digest(), randomSource);
      Commitment com = hashCommit.commit(data);
      commData[0] = com.getCommitment();
      commData[1] = com.getSecret();

      return commData;
    }
    catch (NoSuchAlgorithmException e) {
      throw new CommitException(e);
    }
  }

  /**
   * Generates an ElGamal KeyPair with a size set by the parameter size
   * 
   * @param size
   *          of key pair
   * @return ElGamal KeyPair
   * @throws KeyGenerationException
   */
  public static KeyPair generatorElGamalKeyPair(int size) throws KeyGenerationException {
    try {
      if (!providerAdded) {
        initProvider();
      }

      logger.info("Running KeyPairGenerator with size {}", size);
      KeyPairGenerator generator = KeyPairGenerator.getInstance("ElGamal", CryptoUtils.BC_PROV);
      SecureRandom random = new SecureRandom();
      generator.initialize(size, random);

      return generator.generateKeyPair();
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst generation ElGamal Key Pair (size:{})", e);
      throw new KeyGenerationException("Exception whilst generation ElGamal Key Pair (size:" + size + ")", e);
    }

  }

  /**
   * Create a Cipher object for decryption from an encrypted AES key. It initially decrypts the AES key and then constructs a cipher
   * object with the decrypted AES key. Defaults to AES/ECB/NoPadding
   * 
   * @param encryptedAESKey
   *          JSONObject containing encrypted AESkey, encrypted under ElGamal
   * @param elGamalKeyPair
   *          JSONObject containing ElGamal key pair - we need the private key to do the decryption
   * @return Cipher object instantiated with the decrypted AES key and for decryption
   * @throws CryptoIOException
   */
  public static Cipher getAESDecryptionCipherFromEncKey(JSONObject encryptedAESKey, JSONObject elGamalKeyPair)
      throws CryptoIOException {
    return getAESDecryptionCipherFromEncKey(encryptedAESKey, elGamalKeyPair, DEFAULT_AES_ALGORITHM);
  }

  /**
   * Create a Cipher object for decryption from an encrypted AES key. It initially decrypts the AES key and then constructs a cipher
   * object with the decrypted AES key
   * 
   * @param encryptedAESKey
   *          JSONObject containing encrypted AESkey, encrypted under ElGamal
   * @param elGamalKeyPair
   *          JSONObject containing ElGamal key pair - we need the private key to do the decryption
   * @param algorithm
   *          String containing algorithm to use
   * @return Cipher object instantiated with the decrypted AES key and for decryption
   * @throws CryptoIOException
   */
  public static Cipher getAESDecryptionCipherFromEncKey(JSONObject encryptedAESKey, JSONObject elGamalKeyPair, String algorithm)
      throws CryptoIOException {
    try {
      // Gets AES key decrypted
      byte[] decryptedAESKey = elGamalDecrypt(
          Hex.decodeHex(encryptedAESKey.getString(ClientConstants.AESKeyFile.AES_KEY).toCharArray()),
          CryptoUtils.jsonToPrivateKey(elGamalKeyPair));

      // Prepare AES Cipher object
      SecretKeySpec sks = new SecretKeySpec(decryptedAESKey, "AES");
      Cipher decrypt = Cipher.getInstance(algorithm);
      decrypt.init(Cipher.DECRYPT_MODE, sks);

      return decrypt;
    }
    catch (DecoderException e) {
      throw new CryptoIOException("Exception whilst constructing AES Decrypt Object", e);
    }
    catch (JSONException e) {
      throw new CryptoIOException("Exception whilst constructing AES Decrypt Object", e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new CryptoIOException("Exception whilst constructing AES Decrypt Object", e);
    }
    catch (NoSuchPaddingException e) {
      throw new CryptoIOException("Exception whilst constructing AES Decrypt Object", e);
    }
    catch (InvalidKeyException e) {
      throw new CryptoIOException("Exception whilst constructing AES Decrypt Object", e);
    }
  }

  /**
   * Gets a suitable KeyFactory for the given SignatureType
   * 
   * @param type
   *          SignatureType of the keys to be generated
   * @return KeyFactory of the appropriate type
   * @throws NoSuchAlgorithmException
   * @throws KeyGenerationException
   */
  private static KeyFactory getKeyFactoryFromSigType(SignatureType type) throws NoSuchAlgorithmException, KeyGenerationException {
    switch (type) {
      case DSA:
      case DEFAULT:
        return KeyFactory.getInstance("DSA", CryptoUtils.BC_PROV);
      case ECDSA:
        return KeyFactory.getInstance("ECDSA", CryptoUtils.BC_PROV);
      case BLS:
        throw new KeyGenerationException("Not currently implemented");
      case RSA:
        return KeyFactory.getInstance("RSA", CryptoUtils.BC_PROV);
      default:
        throw new KeyGenerationException("Unknown signature type");
    }
  }

  /**
   * Utility method for retrieving a signature key from a key store
   * 
   * @param keyStorePath
   *          String path to key store
   * @param keyStorePwd
   *          String pass for the key store
   * @param entity
   *          String of the entity name
   * @param entityPwd
   *          String of the entity password
   * @return PrivateKey object retrieved from KeyStore
   * @throws CryptoIOException
   */
  public static PrivateKey getSignatureKeyFromKeyStore(String keyStorePath, String keyStorePwd, String entity, String entityPwd)
      throws CryptoIOException {
    try {
      KeyStore ks = CryptoUtils.loadKeyStore(keyStorePath, keyStorePwd);
      logger.debug("KeystorePath{}", keyStorePath);

      return (PrivateKey) ks.getKey(entity, entityPwd.toCharArray());
    }
    catch (KeyStoreException e) {
      throw new CryptoIOException("Exception whilst accessing keystore", e);
    }
    catch (UnrecoverableKeyException e) {
      throw new CryptoIOException("Exception whilst accessing keystore", e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new CryptoIOException("Exception whilst accessing keystore", e);
    }
  }

  /**
   * Adds the BouncyCastle Security Provider. Can be called several times without causing a problem
   */
  public static void initProvider() {
    if (!providerAdded) {
      logger.info("Adding BouncyCastle provider");
      Security.addProvider(new BouncyCastleProvider());
      providerAdded = true;

      PairingFactory.getInstance().setUsePBCWhenPossible(true);

    }
  }

  /**
   * Converts a JSONObject, containing the fields public and private, components of an ElGamal key pair into a KeyPair object
   * 
   * @param keyPair
   *          JSONObject containing public and private fields
   * @return KeyPair of the JSONObject
   * @throws CryptoIOException
   */
  public static KeyPair jsonToKeyPair(JSONObject keyPair) throws CryptoIOException {
    return new KeyPair(jsonToPublicKey(keyPair), jsonToPrivateKey(keyPair));
  }

  /**
   * Converts a JSONObject, containing the fields public and private, components of an ElGamal key pair into a KeyPair object
   * 
   * @param key
   *          JSONObject containing public and private fields
   * @return KeyPair of the JSONObject
   * @throws CryptoIOException
   */
  public static PrivateKey jsonToPrivateKey(JSONObject key) throws CryptoIOException {
    try {
      // Private key is PKCS8 encoded
      PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(Hex.decodeHex(key.getString("private").toCharArray()));

      // Create a key factory to create the KeyPair object
      KeyFactory keyFactory = KeyFactory.getInstance("ElGamal", new BouncyCastleProvider());
      PrivateKey privKey = keyFactory.generatePrivate(privSpec);
      return privKey;
    }
    catch (DecoderException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (JSONException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (InvalidKeySpecException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
  }

  /**
   * Converts a JSONObject containing a public field into a PublicKey object for ElGamal
   * 
   * @param publicKey
   *          JSONObject with public field
   * @return ElGamal PublicKey object
   * @throws CryptoIOException
   */
  public static PublicKey jsonToPublicKey(JSONObject publicKey) throws CryptoIOException {
    try {
      X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(Hex.decodeHex(publicKey.getString("public").toCharArray()));
      KeyFactory keyFactory = KeyFactory.getInstance("ElGamal", CryptoUtils.BC_PROV);
      PublicKey pubKey = keyFactory.generatePublic(pubSpec);

      return pubKey;
    }
    catch (DecoderException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (JSONException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (InvalidKeySpecException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
  }

  /**
   * Converts a JSONObject, containing the fields public and private, components of an ElGamal key pair into a KeyPair object
   * 
   * @param keyPair
   *          JSONObject containing public and private fields
   * @param type
   *          SignatureType of key that is being loaded
   * @return KeyPair of the JSONObject
   * @throws CryptoIOException
   */
  public static KeyPair jsonToSigningKeyPair(JSONObject keyPair, SignatureType type) throws CryptoIOException {
    return new KeyPair(jsonToSigningPublicKey(keyPair, type), jsonToSigningPrivateKey(keyPair, type));
  }

  /**
   * Converts a JSONObject, containing the fields public and private, components of an ElGamal key pair into a KeyPair object
   * 
   * @param key
   *          JSONObject containing public and private fields
   * @param type
   *          SignatureType of key that is being loaded
   * @return KeyPair of the JSONObject
   * @throws CryptoIOException
   */
  public static PrivateKey jsonToSigningPrivateKey(JSONObject key, SignatureType type) throws CryptoIOException {
    try {
      // Private key is PKCS8 encoded
      PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(IOUtils.decodeData(EncodingType.HEX, key.getString("private")));

      // Create a key factory to create the KeyPair object
      KeyFactory keyFactory = CryptoUtils.getKeyFactoryFromSigType(type);
      PrivateKey privKey = keyFactory.generatePrivate(privSpec);
      return privKey;
    }
    catch (JSONException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (InvalidKeySpecException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (KeyGenerationException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
  }

  /**
   * Converts a JSONObject containing a public field into a PublicKey object for ElGamal
   * 
   * @param publicKey
   *          JSONObject with public field
   * @param type
   *          SignatureType of key that is being loaded
   * @return ElGamal PublicKey object
   * @throws CryptoIOException
   */
  public static PublicKey jsonToSigningPublicKey(JSONObject publicKey, SignatureType type) throws CryptoIOException {
    try {
      X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(IOUtils.decodeData(EncodingType.HEX, publicKey.getString("public")));
      KeyFactory keyFactory = CryptoUtils.getKeyFactoryFromSigType(type);
      PublicKey pubKey = keyFactory.generatePublic(pubSpec);

      return pubKey;
    }
    catch (JSONException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (InvalidKeySpecException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
    catch (KeyGenerationException e) {
      logger.error("Exception whilst converting JSON to KeyPair", e);
      throw new CryptoIOException("Exception whilst converting JSON to KeyPair", e);
    }
  }

  /**
   * Converts a KeyPair object to a JSONObject - useful for saving in a file or transmission across a channel
   * 
   * @param pair
   *          KeyPair to convert to JSON
   * @return JSONObject of the key pair with two fields, private and public
   * @throws CryptoIOException
   */
  public static JSONObject keyPairToJSON(KeyPair pair) throws CryptoIOException {
    try {
      logger.info("Converting KeyPair to JSON");
      JSONObject keyPair = new JSONObject();

      // This gets a PKCS8 encoding of the private key and Hex encodes it
      keyPair.put("private", Hex.encodeHexString(pair.getPrivate().getEncoded()));

      // This gets an x509 encoding of the public key and Hex encodes it
      keyPair.put("public", Hex.encodeHexString(pair.getPublic().getEncoded()));

      return keyPair;
    }
    catch (JSONException e) {
      logger.error("Exception whilst converting key to JSON", e);
      throw new CryptoIOException("Exception whilst converting key to JSON", e);
    }
  }

  /**
   * Loads a KeyStore from the specified path using an empty password
   * 
   * @param path
   *          String path to KeyStore
   * @return KeyStore
   * @throws CryptoIOException
   */
  public static KeyStore loadKeyStore(String path) throws CryptoIOException {
    return loadKeyStore(path, "".toCharArray());
  }

  /**
   * Loads a TVSKeyStore from the specified path using an empty password
   * 
   * @param path
   *          String path to TVSKeyStore
   * @return KeyStore
   * @throws CryptoIOException
   */
  public static TVSKeyStore loadTVSKeyStore(String path) throws CryptoIOException {
    return loadTVSKeyStore(path, "".toCharArray());
  }

  /**
   * Loads a TVSKeyStore from the specified path with the specified password
   * 
   * @param path
   *          String path to KeyStore
   * @param password
   *          char array of password
   * @return KeyStore
   * @throws CryptoIOException
   */
  public static TVSKeyStore loadTVSKeyStore(String path, char[] password) throws CryptoIOException {
    try {
      TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKeyStore.load(path, password);
      return tvsKeyStore;
    }
    catch (KeyStoreException e) {
      logger.error("Exception whilst loading keystore", e);
      throw new CryptoIOException("Exception whilst loading keystore", e);
    }
  }

  /**
   * Loads a KeyStore from the specified path with the specified password
   * 
   * @param path
   *          String path to KeyStore
   * @param pwd
   *          char array of password
   * @return KeyStore
   * @throws CryptoIOException
   */
  public static KeyStore loadKeyStore(String path, char[] pwd) throws CryptoIOException {
    FileInputStream fis = null;

    try {
      // KeyStore ks = KeyStore.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
      KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
      fis = new java.io.FileInputStream(path);
      ks.load(fis, pwd);
      return ks;
    }
    catch (IOException e) {
      logger.error("Exception whilst loading keystore", e);
      throw new CryptoIOException("Exception whilst loading keystore", e);
    }
    catch (KeyStoreException e) {
      logger.error("Exception whilst loading keystore", e);
      throw new CryptoIOException("Exception whilst loading keystore", e);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst loading keystore", e);
      throw new CryptoIOException("Exception whilst loading keystore", e);
    }
    catch (CertificateException e) {
      logger.error("Exception whilst loading keystore", e);
      throw new CryptoIOException("Exception whilst loading keystore", e);
    }
    finally {
      if (fis != null) {
        try {
          fis.close();
        }
        catch (IOException ie) {
          logger.error("Error whilst loading keystore (Closing File)", ie);
          throw new CryptoIOException("Error whilst loading keystore (Closing File)", ie);
        }
      }
    }
  }

  /**
   * Loads a KeyStore from the specified path with the specified password. Utility method to convert the string password to a char
   * array
   * 
   * @param path
   *          String path to KeyStore
   * @param pwd
   *          String of password
   * @return KeyStore
   * @throws CryptoIOException
   */
  public static KeyStore loadKeyStore(String path, String pwd) throws CryptoIOException {
    return loadKeyStore(path, pwd.toCharArray());
  }

  /**
   * Converts an ElGamal PublicKey to a JSONObject with the field Public
   * 
   * @param key
   *          PublicKey to convert
   * @return JSONObject containing a public field
   * @throws CryptoIOException
   */
  public static JSONObject publicKeyToJSON(PublicKey key) throws CryptoIOException {
    try {
      JSONObject keyPair = new JSONObject();
      keyPair.put("public", Hex.encodeHexString(key.getEncoded()));

      return keyPair;
    }
    catch (JSONException e) {
      logger.error("Exception whilst converting key to JSON", e);
      throw new CryptoIOException("Exception whilst converting key to JSON", e);
    }
  }

  /**
   * Stores the KeyStore at the location specified, an empty password will be used
   * 
   * @param ks
   *          KeyStore to write to disk
   * @param path
   *          String path to where the key store should be stored
   * @throws CryptoIOException
   */
  public static void storeKeyStore(KeyStore ks, String path) throws CryptoIOException {
    storeKeyStore(ks, path, "".toCharArray());
  }

  /**
   * Stores the KeyStore at the location specified with the password specified
   * 
   * @param ks
   *          KeyStore to write to disk
   * @param path
   *          String path to where the key store should be stored
   * @param pwd
   *          char array of the password to use
   * @throws CryptoIOException
   */
  public static void storeKeyStore(KeyStore ks, String path, char[] pwd) throws CryptoIOException {
    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream(path);
      ks.store(fos, pwd);
    }
    catch (FileNotFoundException e) {
      logger.error("Exception whilst storing keystore", e);
      throw new CryptoIOException("Exception whilst storing keystore", e);
    }
    catch (KeyStoreException e) {
      logger.error("Exception whilst storing keystore", e);
      throw new CryptoIOException("Exception whilst storing keystore", e);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst storing keystore", e);
      throw new CryptoIOException("Exception whilst storing keystore", e);
    }
    catch (CertificateException e) {
      logger.error("Exception whilst storing keystore", e);
      throw new CryptoIOException("Exception whilst storing keystore", e);
    }
    catch (IOException e) {
      logger.error("Exception whilst storing keystore", e);
      throw new CryptoIOException("Exception whilst storing keystore", e);
    }
    finally {
      if (fos != null) {
        try {
          fos.close();
        }
        catch (IOException ie) {
          logger.error("Error whilst storing keystore (Closing File)", ie);
          throw new CryptoIOException("Error whilst storing keystore (Closing File)", ie);
        }
      }
    }
  }

  /**
   * Stores the KeyStore at the location specified with the password specified. This is a utlity method that converts the string
   * password into a char array
   * 
   * @param ks
   *          KeyStore to write to disk
   * @param path
   *          String path to where the key store should be stored
   * @param pwd
   *          String password to use
   * @throws CryptoIOException
   */
  public static void storeKeyStore(KeyStore ks, String path, String pwd) throws CryptoIOException {
    storeKeyStore(ks, path, pwd.toCharArray());
  }
}
