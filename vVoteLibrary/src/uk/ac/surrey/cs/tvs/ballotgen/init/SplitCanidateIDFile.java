/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.init;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;

import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Utility for splitting Candidate ID files into different races
 * 
 * @author Chris Culnane
 * 
 */
public class SplitCanidateIDFile {

  /**
   * Default constructor
   */
  public SplitCanidateIDFile() {

  }

  /**
   * Main method to perform the actual split
   * 
   * @param args
   *          String arrays of arguements as specified in printUsage
   * @throws JSONIOException
   * @throws JSONException
   */
  public static void main(String[] args) throws JSONIOException, JSONException {
    if (args.length < 4) {
      printUsage();
    }
    File input = new File(args[0]);
    String[] prefix = new String[] { "LA_", "LC_ATL_", "LC_BTL_" };
    JSONArray candIDs = IOUtils.readJSONArrayFromFile(args[0]);
    int start = 0;
    for (int i = 1; i < 4; i++) {
      JSONArray output = new JSONArray();

      if (i > 1) {
        start = start + Integer.parseInt(args[i - 1]);
      }
      for (int j = start; j < start + Integer.parseInt(args[i]); j++) {
        output.put(candIDs.get(j));

      }
      File outputFile = new File(input.getParentFile(), prefix[i - 1] + input.getName());
      IOUtils.writeJSONToFile(output, outputFile.getAbsolutePath());
    }

  }

  /**
   * Displays the usage information for the class when run from the command line.
   */
  private static final void printUsage() {
    System.out.println("Usage of SplitCanidateIDFile:");
    System.out.println("SplitCanidateIDFile provide a utility function for");
    System.out.println("splitting a full list of candidate IDs into separate files");
    System.out.println("for each race:\n");
    System.out.println("The output file will be in the same location as the");
    System.out.println("pathToCandIDFile with an appropriate prefix (la,lcatl,lcbtl)");
    System.out.println("SplitCanidateIDFile pathToCandIDFile numLACands numLCATLCands numLCBTLCands\n");
    System.out.println("\tpathToCandIDFile: path to full list");
    System.out.println("\tnumLACands: number of la candidates");
    System.out.println("\tnumLCATLCands: number of lc_atl candidates");
    System.out.println("\tnumLCBTLCands: number of lc_btl candidates");
  }

}
