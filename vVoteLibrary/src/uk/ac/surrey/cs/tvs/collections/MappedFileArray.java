/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.collections;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.PriorityQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Provides a File backed List using MappedByteBuffers to provide efficient access to the underlying file. Can be used for both
 * temporary and permanent file creation.
 * 
 * @author Chris Culnane
 * 
 */
public class MappedFileArray<A> implements List<A> {

  /**
   * The number of rows held in memory before being written to disk. This will also be the size of each page in the Mapped File
   */
  private static final int                 DEFAULT_IN_MEMORY_LIMIT = 10000;
  /**
   * Logger
   */
  private static final Logger              logger                  = LoggerFactory.getLogger(MappedFileArray.class);

  /**
   * ArrayList of byte[] entries that contains the in-memory portion of the list
   */
  private ArrayList<byte[]>                buffer                  = new ArrayList<byte[]>();

  /**
   * Reference to the MappedFileArrayEntryConverter that is used to encode and decode objects to the underlying data structure
   */
  private MappedFileArrayEntryConverter<A> converter;

  /**
   * Current max length of an entry in the in-memory array
   */
  private int                              currentMaxLineLength    = 0;

  /**
   * Current size of the entire list, not just the in-memory portion
   */
  private long                             currentSize             = 0;


  /**
   * volatile long to record heap sort progress
   */
  private volatile long                    heapSortProgress;
  /**
   * FileChannel that refers to the underlying file
   */
  private FileChannel                      fc;

  /**
   * The in memory limit
   */
  private int                              inMemoryLimit           = DEFAULT_IN_MEMORY_LIMIT;

  /**
   * File that is used to hold the reference to the map file
   */
  private File                             mapFile;

  /**
   * File that points to the folder where new mapped files should be generated
   */
  private File                             mappedFileDir           = null;

  /**
   * Boolean that determines if a mergeSort is likely to be used - if true the pages will be sorted prior to being written to disk
   */
  private boolean                          mergeSort               = false;

  /**
   * The next position to create a page in the underlying file
   */
  private long                             nextPosition            = 0;

  /**
   * Overall max length of an entry across all pages and in-memory
   */
  private int                              overallMaxLineLength    = 0;

  /**
   * ArrayList of PagedArrayList - each one represents a page in the larger file
   */
  private ArrayList<PagedArrayList>        pages                   = new ArrayList<PagedArrayList>();

  /**
   * Boolean that determines if the file backed list is to be persistent
   */
  private boolean                          permanent               = false;

  /**
   * File that points to the permanent file if the array is to be persistent
   */
  private File                             permanentFile           = null;

  /**
   * The RandomAccessFile that points to the actual file being used
   */
  private RandomAccessFile                 racFile;

  /**
   * Boolean that records if a resize will be required prior to a sort
   */
  private boolean                          resizeRequired          = false;



  /**
   * volatile long to record heapify progress
   */
  private volatile long                    heapifyProgress;

  /**
   * Creates a new MappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter specified. This
   * will use a default in memory limit, the underlying files used will not be persistent. This assumes a mergesort will not be
   * required and hence does not pre-sort the memory mapped lists before writing them to disk
   * 
   * @param tempDir
   *          File that points to the temporary directory to use
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @throws IOException
   * 
   */
  public MappedFileArray(File tempDir, MappedFileArrayEntryConverter<A> converter) throws IOException {
    this(DEFAULT_IN_MEMORY_LIMIT, tempDir, converter, false, false, null);
  }

  /**
   * Creates a new MappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter specified. This
   * will use the specified in memory limit, the underlying files used will not be persistent. This assumes a mergesort will not be
   * required and hence does not pre-sort the memory mapped lists before writing them to disk
   * 
   * @param inMemoryLimit
   *          int specifying the limit on in memory data
   * @param tempDir
   *          File that points to the temporary directory to use
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @throws IOException
   */
  public MappedFileArray(int inMemoryLimit, File tempDir, MappedFileArrayEntryConverter<A> converter) throws IOException {
    this(inMemoryLimit, tempDir, converter, false, false, null);
  }

  /**
   * Creates a new MappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter specified. This
   * will use the specified in memory limit and will pre-sort lists prior to writing to disk if mergesort is set to true. This will
   * not default to not using persistent storage.
   * 
   * @param inMemory
   *          Limit int specifying the limit on in memory data
   * @param tempDir
   *          File that points to the temporary directory to use
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @param mergeSort
   *          boolean, true if we expect to perform a merge sort, false if not
   * @throws IOException
   */
  public MappedFileArray(int inMemoryLimit, File tempDir, MappedFileArrayEntryConverter<A> converter, boolean mergeSort)
      throws IOException {
    this(inMemoryLimit, tempDir, converter, mergeSort, false, null);
  }

  /**
   * Creates a new MappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter specified. This
   * will use the specified in memory limit. The persistence and mergesort parameters are as specified.
   * 
   * @param inMemoryLimit
   *          int specifying the limit on in memory data
   * @param tempDir
   *          File that points to the temporary directory to use
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @param mergeSort
   *          boolean, true if we expect to perform a merge sort, false if not
   * @param permanent
   *          boolean, true if the underlying files are to be kept, false if not
   * @param permanentFile
   *          File that points to the permanent file to use for persistent storage, null if permanent is false
   * @throws IOException
   */
  public MappedFileArray(int inMemoryLimit, File tempDir, MappedFileArrayEntryConverter<A> converter, boolean mergeSort,
      boolean permanent, File permanentFile) throws IOException {
    logger.info("Creating new MappedFileArray inMemoryLimit:{},mergeSort:{},permanent:{}", inMemoryLimit, mergeSort, permanent);
    this.inMemoryLimit = inMemoryLimit;
    this.mappedFileDir = tempDir;
    this.converter = converter;
    this.mergeSort = mergeSort;
    this.mapFile = tempDir;
    this.overallMaxLineLength = this.converter.getInitialLineLength();
    this.permanent = permanent;
    this.permanentFile = permanentFile;
    this.init();

  }

  /**
   * Creates a new MappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter specified. This
   * will use the specified in memory limit, it will not pre-sort lists prior to writing to disk and will not default to not using
   * persistent storage.
   * 
   * @param inMemoryLimit
   *          int specifying the limit on in memory data
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @throws IOException
   */
  public MappedFileArray(int inMemoryLimit, MappedFileArrayEntryConverter<A> converter) throws IOException {
    this(inMemoryLimit, null, converter, false, false, null);
  }

  /**
   * Creates a new MappedFileArray in the default temporary directory specified, using the MappedFileArrayEntryConverter specified.
   * This will use the default in memory limit, it will not pre-sort lists prior to writing to disk and will not default to not
   * using persistent storage.
   * 
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @throws IOException
   */
  public MappedFileArray(MappedFileArrayEntryConverter<A> converter) throws IOException {
    this(DEFAULT_IN_MEMORY_LIMIT, null, converter, false, false, null);

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#add(java.lang.Object)
   */
  @Override
  public boolean add(A e) {
    return this.add(this.converter.encode(e));

  }

  /**
   * Internal method to actually add an object to the list. We have this implementation to improve efficiency. When performing
   * internal operations we can do add and removes directly on the bytes, without needing to encode and decode. This is particularly
   * advantageous when performing the merge sort, which involves adding and removing a large number of items.
   * 
   * @param e
   *          byte array of the element to be added
   * @return boolean true
   */
  private boolean add(byte[] e) {
    try {
      // Add it to the in-memory buffer
      this.buffer.add(e);
      this.currentSize++;

      // Check if the maximum line length has increased
      this.currentMaxLineLength = Math.max(this.currentMaxLineLength, e.length);

      // Check if the buffer size has exceeded the in memory limit
      if (this.buffer.size() == this.inMemoryLimit) {
        // If it is has write the buffer to disk
        if (this.mergeSort) {
          // If we think a merge sort will take place we pre-sort the in-memory list
          Collections.sort(this.buffer, this.converter.getComparator());
        }
        // Check if a resize is required
        if (this.currentMaxLineLength > this.converter.getInitialLineLength()) {
          this.resizeRequired = true;
        }

        // Record the ovreall max line length of the whole array
        this.overallMaxLineLength = Math.max(this.overallMaxLineLength, this.currentMaxLineLength);

        // Create new PagedArrayList and add it to the collection of lists held internally
        PagedArrayList pal = this.addMappedArray(this.inMemoryLimit, this.overallMaxLineLength);

        // Add the in-memory buffer to the new PagedArrayList and clear the in-memory buffer
        pal.addAll(this.buffer);
        this.buffer.clear();

        this.currentMaxLineLength = 0;
      }
    }
    catch (IOException ex) {
      // We cannot recover from this
      throw new RuntimeException("Exception adding message", ex);
    }
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#add(int, java.lang.Object)
   */
  @Override
  public void add(int index, A element) {
    throw new UnsupportedOperationException("Insert operations are not supported for file backed arrays");

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#addAll(java.util.Collection)
   */
  @Override
  public boolean addAll(Collection<? extends A> c) {
    for (A entry : c) {
      this.add(entry);
    }
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#addAll(int, java.util.Collection)
   */
  @Override
  public boolean addAll(int index, Collection<? extends A> c) {
    throw new UnsupportedOperationException("Insert operations are not supported for file backed arrays");
  }

  /**
   * Creates and adds a new PagedArrayList with the specified number of rows and line length.
   * 
   * @param rows
   *          int representing the number of rows
   * @param lineLength
   *          int representing the line length in bytes
   * @return PagedArrayList new empty PagedArrayList
   * @throws IOException
   */
  private PagedArrayList addMappedArray(int rows, int lineLength) throws IOException {
    MappedByteBuffer mappedBuffer = this.fc.map(MapMode.READ_WRITE, this.nextPosition, (long) rows * (long) lineLength);
    PagedArrayList page = new PagedArrayList(mappedBuffer, rows, lineLength);
    this.pages.add(page);
    this.nextPosition = this.nextPosition + ((long) rows * (long) lineLength);
    return page;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#clear()
   */
  @Override
  public void clear() {
    throw new UnsupportedOperationException("Remove operations are not supported for file backed arrays");
  }

  /**
   * Closes the underlying file but does not delete it. This is used where you want to keep the file for later reloading. This can
   * only be called if the list was declared as permanent on instantiation, otherwise an IOException will be thrown.
   * 
   * @throws IOException
   */
  public void closeAndKeep() throws IOException {
    if (!this.permanent) {
      throw new IOException("List was not declared as permanent when instantiated");
    }
    if (this.resizeRequired) {
      this.resizeArray();
    }
    if (this.mergeSort) {
      Collections.sort(this.buffer, this.converter.getComparator());
    }
    if (this.currentMaxLineLength > this.converter.getInitialLineLength()) {
      this.resizeRequired = true;
    }
    this.overallMaxLineLength = Math.max(this.overallMaxLineLength, this.currentMaxLineLength);
    PagedArrayList pal = this.addMappedArray(this.buffer.size(), this.overallMaxLineLength);
    pal.addAll(this.buffer);
    this.buffer.clear();
    this.currentMaxLineLength = 0;
    logger.info("closeAndKeep has been called, will flush pages and close files");
    for (PagedArrayList page : this.pages) {
      page.forceToDisk();
    }

    this.racFile.close();

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#contains(java.lang.Object)
   */
  @Override
  public boolean contains(Object o) {
    throw new UnsupportedOperationException("Searching is not currently supported");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#containsAll(java.util.Collection)
   */
  @Override
  public boolean containsAll(Collection<?> c) {
    throw new UnsupportedOperationException("Searching is not currently supported");
  }

  /**
   * Disposes of this MappedFileArray, forcing any paged data to be written to disk. It closes the underlying file and if not
   * permanent, it will delete the on disk file.
   * 
   * @throws IOException
   */
  public void dispose() throws IOException {
    logger.info("Dispose has been called, will flush pages and close files");
    for (PagedArrayList page : this.pages) {
      page.forceToDisk();
    }

    this.racFile.close();
    if (!this.permanent) {
      IOUtils.checkAndDeleteFile(this.mapFile);
    }

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#get(int)
   */
  @Override
  public A get(int index) {
    // Calculate page of item, if it is in-memory return from buffer, otherwise retrieve from PageMappedMemory
    int page = index / this.inMemoryLimit;
    if (page >= this.pages.size()) {
      return this.converter.decode(this.buffer.get(index % this.inMemoryLimit));
    }
    byte[] data = this.pages.get(page).get(index % this.inMemoryLimit);
    return this.converter.decode(data);

  }

  /**
   * Returns the element at the specified long index
   * 
   * @param index
   *          long index
   * @return the element at the specified position
   */
  public A get(long index) {
    // Calculate page of item, if it is in-memory return from buffer, otherwise retrieve from PageMappedMemory
    int page = (int) index / this.inMemoryLimit;
    if (page >= this.pages.size()) {
      return this.converter.decode(this.buffer.get((int) (index % this.inMemoryLimit)));
    }
    byte[] data = this.pages.get(page).get((int) (index % this.inMemoryLimit));
    return this.converter.decode(data);

  }

  /**
   * Gets the current in-memory portion of the list
   * 
   * @return ArrayList of byte arrays containing the in-memory portion of the list
   */
  public ArrayList<byte[]> getCurentBuffer() {
    return this.buffer;
  }

  /**
   * Returns the progress of the heapify method (precursor to heap sort). It returns the start value which gradually decreases to
   * zero
   * 
   * @return long of the current heapfiy value
   */
  public long getHeapifyProgress() {
    return this.heapifyProgress;
  }

  /**
   * Returns the progress of the actual heap sort. When the returned value reaches zero the heap sort is complete
   * 
   * @return long of the current heap sort index
   */
  public long getHeapSortEnd() {
    return this.heapSortProgress;
  }

  /**
   * Gets the File that points to the underlying Map File
   * 
   * @return File that points to the underlying Map File
   */
  public File getMapFile() {
    return this.mapFile;
  }

  /**
   * Gets a List of PagedArrayList objects that contains the pages within this MappedFileArray
   * 
   * @return List of PagedArrayList page objects
   */
  public List<PagedArrayList> getPages() {
    return this.pages;
  }

  /**
   * Gets a raw byte value from the PagedArrayList. This method is private because it is only used internally. We access the raw
   * data during sort operations as it is far more efficient that encoding and decoding.
   * 
   * @param index
   *          long of the index to return
   * @return byte array containing the value at that location
   */
  private byte[] getRaw(long index) {
    int page = (int) (index / this.inMemoryLimit);
    if (page >= this.pages.size()) {
      return this.buffer.get((int) (index % this.inMemoryLimit));
    }
    return this.pages.get(page).get((int) (index % this.inMemoryLimit));

  }

  /**
   * Internal method to build the heap with the largest element as the root
   */
  private void heapify() {
    int count = this.size();
    // start is assigned the index in a of the last parent node
    long start = (count - 2) / 2; // binary heap
    this.heapifyProgress=start;
    while (start >= 0) {
      // sift down the node at index start to the proper place
      // such that all nodes below the start index are in heap
      // order
      this.siftDown(start, count - 1);
      start--;
      this.heapifyProgress=start;
    }
    // after sifting down the root all nodes/elements are in heap order
  }

  /**
   * Perform a heapsort on the array by first constructing a heap with the largest element as the root and then performing the sort
   */
  public void heapSort() {

    if (this.resizeRequired) {
      this.resizeArray();
    }
    int count = this.size();

    // first place a in max-heap order
    this.heapify();

    long end = count - 1;
    this.heapSortProgress=end;
    while (end > 0) {
      this.swap(end, 0);
      // put the heap back in max-heap order
      this.siftDown(0, end - 1);
      // decrement the size of the heap so that the previous
      // max value will stay in its proper place
      end--;
      this.heapSortProgress=end;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#indexOf(java.lang.Object)
   */
  @Override
  public int indexOf(Object o) {
    throw new UnsupportedOperationException("Searching is not currently supported");
  }

  /**
   * Internal method to initialise the MappedFileArray. This creates any temporary files and instantiates the underlying
   * RandomAccessFile
   * 
   * @throws IOException
   */
  private void init() throws IOException {
    if (this.permanent && this.permanentFile != null) {
      logger.info("MappedFileArray is set to permanent:{}", this.permanentFile);
      this.mapFile = this.permanentFile;
    }
    else if (this.mappedFileDir != null) {
      this.mapFile = File.createTempFile("mfa", "mfa", this.mappedFileDir);
      logger.info("Created temp file in specified location:{}", this.mapFile);
    }
    else {
      this.mapFile = File.createTempFile("mfa", "mfa");
      logger.info("Created temp file in default location:{}", this.mapFile);
    }
    if (!this.permanent) {
      this.mapFile.deleteOnExit();
    }
    this.racFile = new RandomAccessFile(this.mapFile, "rw");
    this.fc = this.racFile.getChannel();

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#isEmpty()
   */
  @Override
  public boolean isEmpty() {
    if (this.currentSize == 0) {
      return true;
    }
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#iterator()
   */
  @Override
  public Iterator<A> iterator() {
    return new MappedFileArrayListIterator<A>(this);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#lastIndexOf(java.lang.Object)
   */
  @Override
  public int lastIndexOf(Object o) {
    throw new UnsupportedOperationException("Searching is not currently supported");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#listIterator()
   */
  @Override
  public ListIterator<A> listIterator() {
    return new MappedFileArrayListIterator<A>(this);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#listIterator(int)
   */
  @Override
  public ListIterator<A> listIterator(int index) {
    return new MappedFileArrayListIterator<A>(this, index);
  }

  /**
   * Reloads the underlying MappedFileArray from the specified file.
   * 
   * @throws IOException
   */
  public void reloadFromFile() throws IOException {
    logger.info("Reloading file with length {}", this.racFile.length());
    // Check the configuration is consistent with the underlying file, the length must be divisible by the overallMaxLineLength
    if ((this.racFile.length() % (this.overallMaxLineLength)) != 0) {
      throw new IOException("Mapped configuration does not match file structure");
    }
    long idx = 0;
    // Load data into in PagedArrayLists
    while (idx < this.racFile.length()) {
      long sizeCount = this.inMemoryLimit;
      if ((this.racFile.length() - idx) < this.overallMaxLineLength * this.inMemoryLimit) {
        sizeCount = (this.racFile.length() - idx) / this.overallMaxLineLength;
      }

      MappedByteBuffer mappedBuffer = this.fc.map(MapMode.READ_WRITE, idx, sizeCount * this.overallMaxLineLength);
      PagedArrayList page = new PagedArrayList(mappedBuffer, this.inMemoryLimit, this.overallMaxLineLength, (int) sizeCount);
      this.currentSize = this.currentSize + sizeCount;
      if (page.size() == this.inMemoryLimit) {
        this.pages.add(page);
      }
      else {
        this.buffer.addAll(page);
      }
      idx = idx + (sizeCount * this.overallMaxLineLength);
    }
    // Reset next index position
    this.nextPosition = idx;

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#remove(int)
   */
  @Override
  public A remove(int index) {
    throw new UnsupportedOperationException("Remove operations are not supported for file backed arrays");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#remove(java.lang.Object)
   */
  @Override
  public boolean remove(Object o) {
    throw new UnsupportedOperationException("Remove operations are not supported for file backed arrays");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#removeAll(java.util.Collection)
   */
  @Override
  public boolean removeAll(Collection<?> c) {
    throw new UnsupportedOperationException("Remove operations are not supported for file backed arrays");
  }

  /**
   * Internal method to call the resizeArray method with the preset overallMaxlineLength
   */
  private void resizeArray() {
    this.resizeArray(this.overallMaxLineLength);
  }

  /**
   * Resizes the underlying array with the new specified size for each line. Due to the way data is stored in the underlying file
   * each element must be the same length. In cases where they are shorter they are padded. If a longer element is added to the
   * array the rest of the array must be resized and padded to that size.
   * 
   * @param newlength
   *          int of the next element length
   */
  private void resizeArray(int newlength) {
    logger.info("Resizing array elements to {}", newlength);
    int newLineLength = newlength;
    long lengthOfFile = this.pages.size() * (long) this.inMemoryLimit * (long) newLineLength;
    long nextPos = lengthOfFile;
    // Start at the last page and resize the elements
    for (int pageIdx = this.pages.size() - 1; pageIdx >= 0; pageIdx--) {
      PagedArrayList pagedArray = this.pages.get(pageIdx);
      nextPos = nextPos - ((long)this.inMemoryLimit * (long)newLineLength);
      MappedByteBuffer mappedBuffer;
      try {
        mappedBuffer = this.fc.map(MapMode.READ_WRITE, nextPos, (long)this.inMemoryLimit * (long)newLineLength);
        pagedArray.resizeRowLength(this.overallMaxLineLength, mappedBuffer);
      }
      catch (IOException e) {
        throw new RuntimeException("Exception resizing underlying paged array", e);
      }

    }
    this.resizeRequired = false;
    this.currentMaxLineLength = newLineLength;
    this.nextPosition = lengthOfFile;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#retainAll(java.util.Collection)
   */
  @Override
  public boolean retainAll(Collection<?> c) {
    throw new UnsupportedOperationException("Remove operations are not supported for file backed arrays");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#set(int, java.lang.Object)
   */
  @Override
  public A set(int index, A element) {
    return this.converter.decode(this.set(index, this.converter.encode(element), true));

  }

  /**
   * Internal method to set a raw byte array element at a particular index
   * 
   * @param index
   *          int index of element to set
   * @param element
   *          byte array of data to be set
   * @return byte array of the previous element at that index
   */
  private byte[] set(long index, byte[] element) {
    return this.set(index, element, false);

  }

  /**
   * Internal method to set a raw byte array element at a particular index
   * 
   * @param index
   *          int index of element to set
   * @param element
   *          byte array of data to be set
   * @param publicCall
   *          boolean indicating if this call originated from a public method
   * @return byte array of the previous element at that index
   */
  private byte[] set(long index, byte[] element, boolean publicCall) {
    int page = (int) (index / this.inMemoryLimit);
    if (page >= this.pages.size()) {
      return this.buffer.set((int) (index % this.inMemoryLimit), element);
    }
    // We only want to check when called externally, other calls to set are private and should be covered by the single check
    // performed before the sort
    if (publicCall && this.pages.get(page).getLineLength() < element.length) {
      this.overallMaxLineLength = Math.max(this.overallMaxLineLength, element.length);
      this.resizeArray();
    }
    return this.pages.get(page).set((int) (index % this.inMemoryLimit), element);
  }

  /**
   * Part of the heapify method to order the elements in the heap
   * 
   * @param start
   *          int index to start at
   * @param end
   *          int index to end at
   */
  private synchronized void siftDown(long start, long end) {
    // end represents the limit of how far down the heap to sift
    long root = start;

    while ((root * 2 + 1) <= end) { // While the root has at least one child
      long child = root * 2 + 1; // root*2+1 points to the left child
      // if the child has a sibling and the child's value is less than its sibling's...

      if (child + 1 <= end && this.converter.compare(this.getRaw(child), (this.getRaw(child + 1))) < 0) {
        child = child + 1; // ... then point to the right child instead
      }
      if (this.converter.compare(this.getRaw(root), this.getRaw(child)) < 0) { // out of max-heap order
        this.swap(root, child);
        root = child; // repeat to continue sifting down the child now
      }
      else {
        return;
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#size()
   */
  @Override
  public int size() {
    if (((int) this.currentSize) != this.currentSize) {
      throw new RuntimeException("Size is greater than integer");
    }
    return (int) this.currentSize;
  }

  /**
   * Gets the size as a long value to allow handling very large arrays
   * 
   * @return long of the current size
   */
  public long sizeL() {
    return this.currentSize;
  }

  /**
   * Synchronised method to perform a merge sort
   * 
   * @return a new sorted MappedFileArray
   * @throws IOException
   */
  public synchronized MappedFileArray<A> sort() throws IOException {

    Collections.sort(this.buffer, this.converter);
    if (!this.mergeSort) {
      for (PagedArrayList fbal : this.pages) {
        Collections.sort(fbal, this.converter.getComparator());
      }
    }
    PriorityQueue<PagedArrayListIterator> pq = new PriorityQueue<PagedArrayListIterator>(Math.max(1, this.pages.size()),
        new PagedArrayListIteratorComparator(this.converter.getComparator()));

    PagedArrayListIterator itr = new PagedArrayListIterator(this.buffer);
    if (itr.hasNext()) {
      itr.next();
      pq.add(itr);
    }

    for (PagedArrayList fbal : this.pages) {

      itr = (PagedArrayListIterator) fbal.iterator();
      if (itr.hasNext()) {
        itr.next();
        pq.add(itr);
      }

    }

    PagedArrayListIterator currentIterator;
    MappedFileArray<A> sortedCommitArray;
    if (this.mappedFileDir != null) {

    }

    sortedCommitArray = new MappedFileArray<A>(this.inMemoryLimit, this.mappedFileDir, this.converter, this.mergeSort,
        this.permanent, null);

    // Loop through until the PriorityQueue is empty (once all data from all files has been read). The poll method removes the top
    // most file
    while ((currentIterator = pq.poll()) != null) {
      // If the current line is null we have reached the end of that file, do nothing, since we have already removed it from the
      // PriorityQueue
      if (currentIterator.current() != null) {

        sortedCommitArray.add(currentIterator.current());

        if (currentIterator.hasNext()) {
          currentIterator.next();
          pq.add(currentIterator);
        }
      }
    }
    return sortedCommitArray;

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#subList(int, int)
   */
  @Override
  public List<A> subList(int fromIndex, int toIndex) {
    throw new UnsupportedOperationException("Sublist are not supported for file backed arrays");
  }

  /**
   * Swaps to elements with each other. Internal method used during the heap sort, we operate directly on the byte values to avoid
   * encoding and decoding costs
   * 
   * @param idxA
   *          index of first element (A) to be swapped with element B
   * @param idxB
   *          index of second element (B) to be swapped with element A
   */
  private void swap(long idxA, long idxB) {
    this.set(idxA, this.set(idxB, this.getRaw(idxA)));
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#toArray()
   */
  @Override
  public Object[] toArray() {
    Object[] outputArray = new Object[this.size()];
    for (int i = 0; i < this.size(); i++) {
      outputArray[i] = this.get(i);
    }
    return outputArray;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#toArray(java.lang.Object[])
   */
  @SuppressWarnings("unchecked")
  @Override
  public <T> T[] toArray(T[] a) {
    if (a.length < this.size()) {
      a = Arrays.copyOf(a, this.size());
    }
    for (int i = 0; i < this.size(); i++) {
      a[i] = (T) this.get(i);
    }
    if (a.length > this.size()) {
      a[this.size()] = null;
    }
    return a;
  }

  /**
   * Truncates the underlying array to the new length. This is an important final step to reduce overall file size on large tables
   * 
   * @param newlength
   *          int new length of each element
   * @throws IOException
   */
  public void truncateArray(int newlength) throws IOException {
    logger.info("Truncating array");
    int newLineLength = newlength;
    long lengthOfFile = (long) this.pages.size() * (long) this.inMemoryLimit * newLineLength;
    long nextPos = 0;
    for (int pageIdx = 0; pageIdx < this.pages.size(); pageIdx++) {
      PagedArrayList pagedArray = this.pages.get(pageIdx);
      MappedByteBuffer mappedBuffer;
      try {
        mappedBuffer = this.fc.map(MapMode.READ_WRITE, nextPos, (long) this.inMemoryLimit * (long) newLineLength);
        pagedArray.resizeRowLengthTruncate(newLineLength, mappedBuffer);
        // Added to try and force garbage collection of old MappedBuffers
        System.gc();
        System.runFinalization();
      }
      catch (IOException e) {
        throw new RuntimeException("Exception resizing underlying paged array", e);
      }
      nextPos = nextPos + ((long)this.inMemoryLimit * (long)newLineLength);

    }
    this.resizeRequired = false;
    this.currentMaxLineLength = newLineLength;
    this.overallMaxLineLength = newLineLength;
    this.nextPosition = lengthOfFile;
    this.racFile.setLength(lengthOfFile);
    // resize to overmaxlength
  }
}
