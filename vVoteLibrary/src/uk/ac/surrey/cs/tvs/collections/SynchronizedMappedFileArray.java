/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation

 ******************************************************************************/
package uk.ac.surrey.cs.tvs.collections;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * Synchronised wrapper for a MappedFileArray. Provides thread-safe methods for interacting with the underlying MappedFileArray.
 * 
 * @author Chris Culnane
 * 
 */
public class SynchronizedMappedFileArray<A> extends MappedFileArray<A> {

  /**
   * Creates a new SynchronizedMappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter
   * specified. This will use a default in memory limit, the underlying files used will not be persistent. This assumes a mergesort
   * will not be required and hence does not pre-sort the memory mapped lists before writing them to disk
   * 
   * @param tempDir
   *          File that points to the temporary directory to use
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @throws IOException
   * 
   */
  public SynchronizedMappedFileArray(File tempDir, MappedFileArrayEntryConverter<A> converter) throws IOException {
    super(tempDir, converter);
  }

  /**
   * Creates a new SynchronizedMappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter
   * specified. This will use the specified in memory limit, the underlying files used will not be persistent. This assumes a
   * mergesort will not be required and hence does not pre-sort the memory mapped lists before writing them to disk
   * 
   * @param inMemoryLimit
   *          int specifying the limit on in memory data
   * @param tempDir
   *          File that points to the temporary directory to use
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @throws IOException
   */
  public SynchronizedMappedFileArray(int inMemoryLimit, File tempDir, MappedFileArrayEntryConverter<A> converter)
      throws IOException {
    super(inMemoryLimit, tempDir, converter);
  }

  /**
   * Creates a new SynchronizedMappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter
   * specified. This will use the specified in memory limit and will pre-sort lists prior to writing to disk if mergesort is set to
   * true. This will not default to not using persistent storage.
   * 
   * @param inMemory
   *          Limit int specifying the limit on in memory data
   * @param tempDir
   *          File that points to the temporary directory to use
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @param mergeSort
   *          boolean, true if we expect to perform a merge sort, false if not
   * @throws IOException
   */
  public SynchronizedMappedFileArray(int inMemoryLimit, File tempDir, MappedFileArrayEntryConverter<A> converter, boolean mergeSort)
      throws IOException {
    super(inMemoryLimit, tempDir, converter, mergeSort);
  }

  /**
   * Creates a new SynchronizedMappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter
   * specified. This will use the specified in memory limit. The persistence and mergesort parameters are as specified.
   * 
   * @param inMemoryLimit
   *          int specifying the limit on in memory data
   * @param tempDir
   *          File that points to the temporary directory to use
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @param mergeSort
   *          boolean, true if we expect to perform a merge sort, false if not
   * @param permanent
   *          boolean, true if the underlying files are to be kept, false if not
   * @param permanentFile
   *          File that points to the permanent file to use for persistent storage, null if permanent is false
   * @throws IOException
   */
  public SynchronizedMappedFileArray(int inMemoryLimit, File tempDir, MappedFileArrayEntryConverter<A> converter,
      boolean mergeSort, boolean permanent, File permanentFile) throws IOException {
    super(inMemoryLimit, tempDir, converter, mergeSort, permanent, permanentFile);
  }

  /**
   * Creates a new SynchronizedMappedFileArray in the temporary directory specified, using the MappedFileArrayEntryConverter
   * specified. This will use the specified in memory limit, it will not pre-sort lists prior to writing to disk and will not
   * default to not using persistent storage.
   * 
   * @param inMemoryLimit
   *          int specifying the limit on in memory data
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @throws IOException
   */
  public SynchronizedMappedFileArray(int inMemoryLimit, MappedFileArrayEntryConverter<A> converter) throws IOException {
    super(inMemoryLimit, converter);

  }

  /**
   * Creates a new SynchronizedMappedFileArray in the default temporary directory specified, using the MappedFileArrayEntryConverter
   * specified. This will use the default in memory limit, it will not pre-sort lists prior to writing to disk and will not default
   * to not using persistent storage.
   * 
   * @param converter
   *          MappedFileArrayEntryConverter to be used when mapping between generic and concrete entries
   * @throws IOException
   */
  public SynchronizedMappedFileArray(MappedFileArrayEntryConverter<A> converter) throws IOException {
    super(converter);
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.collections.MappedFileArray#add(java.lang.Object)
   */
  @Override
  public synchronized boolean add(A e) {
    return super.add(e);
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.collections.MappedFileArray#addAll(java.util.Collection)
   */
  @Override
  public synchronized boolean addAll(Collection<? extends A> c) {
    return super.addAll(c);
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.collections.MappedFileArray#get(int)
   */
  @Override
  public synchronized A get(int index) {
    return super.get(index);
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.collections.MappedFileArray#set(int, java.lang.Object)
   */
  @Override
  public synchronized A set(int index, A element) {
    return super.set(index, element);
  }

}
