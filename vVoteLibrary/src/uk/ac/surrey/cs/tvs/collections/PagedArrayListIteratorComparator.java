/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation

 ******************************************************************************/
package uk.ac.surrey.cs.tvs.collections;

import java.util.Comparator;

/**
 * Provides a comparator for PagedArrayListIterators. The paged array represents elements as byte arrays, hence all comparisons are
 * done on byte arrays. The compare method utilises the caching of elements in the PagedArrayListIterator to avoid having to
 * repeatedly reload the entire page into the memory. This is useful for performing merge sorts where each page has been sorted
 * during construction and this equates to the merge step.
 * 
 * @author Chris Culnane
 * 
 */
public class PagedArrayListIteratorComparator implements Comparator<PagedArrayListIterator> {

  /**
   * Internal byte array comparator to be used for the underlying elements
   */
  private Comparator<byte[]> itemComparator;

  /**
   * Constructs a new PagedArrayListIteratorComparator to compare PagedArrayListIterators, or more specifically the top elements in
   * each PagedArrayList
   * 
   * @param itemComparator
   *          Comparator for comparing the byte array represents of the underlying elements
   */
  public PagedArrayListIteratorComparator(Comparator<byte[]> itemComparator) {
    this.itemComparator = itemComparator;
  }

  /**
   * It is important to note that this compare method does not advanced or call next on the PagedArrayListIterators passed in. The
   * list iterator should be initialised and the first element retrieved (next called) before performing a comparison. This method
   * will compare the cached current elements of each list iterator and will not change the state of the iterator before or after.
   * It is the responsibility of the calling class to advanced the iterator, based on the comparison. <br><br>
   * 
   * {@inheritDoc}
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   * 
   */
  @Override
  public int compare(PagedArrayListIterator o1, PagedArrayListIterator o2) {
    // Compare the current elements, this does not change the element, that is left to the calling method, hence this uses the
    // cached value.
    return this.itemComparator.compare(o1.current(), o2.current());

  }
}
