/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.collections;

import java.util.Comparator;

/**
 * Abstract class that provides method for encoding, decoding and sorting MappedFileArrayEntries.
 * 
 * @author Chris Culnane
 * 
 */
public abstract class MappedFileArrayEntryConverter<T> implements Comparator<byte[]> {

  /**
   * Decodes a byte array representing an entry into an entry of type <T>
   * 
   * @param entry
   *          byte array containing entry
   * @return entry of type <T>
   */
  public abstract T decode(byte[] entry);

  /**
   * Encodes the entry into a byte array
   * 
   * @param entry
   *          of type <T> to be encoded
   * @return byte array representation of entry
   */
  public abstract byte[] encode(T entry);

  /**
   * Gets a comparator that will compare byte array representations of entries
   * 
   * @return byte array comparator
   */
  public abstract Comparator<byte[]> getComparator();

  /**
   * Gets the initial length of entries of this type
   * 
   * @return int of initial length
   */
  public abstract int getInitialLineLength();
}
