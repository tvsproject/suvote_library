/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.collections;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * A list that is backed by a MappedByteBuffer that file backed. This class is part of the wider tvs.collections package that
 * provides a paged memory mapped list implementation.Records within the MappedByteBuffer are of a fixed length.
 * 
 * @author Chris Culnane
 * 
 */
public class PagedArrayList implements List<byte[]> {

  /**
   * Underlying MappedByteBuffer where the actual data is stored
   */
  private MappedByteBuffer byteBuffer;

  /**
   * current number of elements in this list
   */
  private int              currentSize = 0;

  /**
   * Empty byte buffer, will be initialised with preset length to improve padding efficiency
   */
  private byte[]           empty;

  /**
   * Number of rows that this paged list will hold - is limited due to fixed length within larger file
   */
  private int              rowCount;

  /**
   * byte length of each row
   */
  private int              rowLength;

  /**
   * Creates a new PagedArrayList with the underlying MappedByteBuffer consisting n rows of length rowLength
   * 
   * @param byteBuffer
   *          MappedByteBuffer to use for storage
   * @param rows
   *          int number of rows in this page
   * @param rowLength
   *          int length of each row
   * @throws IOException
   */
  public PagedArrayList(MappedByteBuffer byteBuffer, int rows, int rowLength) throws IOException {
    this.rowLength = rowLength;
    this.rowCount = rows;
    this.byteBuffer = byteBuffer;
    this.empty = new byte[rowLength];
  }

  /**
   * Creates a new PagedArrayList with the underlying MappedByteBuffer that may already contain data. The array will consist of n
   * rows each of length rowLength. The current number of rows is determined by currentSize.
   * 
   * @param byteBuffer
   *          MappedByteBuffer to use for storage
   * @param rows
   *          int number of rows in this page
   * @param rowLength
   *          int length of each row
   * @param currentSize
   *          the current number of rows already in the buffer
   * @throws IOException
   */
  public PagedArrayList(MappedByteBuffer byteBuffer, int rows, int rowLength, int currentSize) throws IOException {
    this.rowLength = rowLength;
    this.rowCount = rows;
    this.byteBuffer = byteBuffer;
    this.empty = new byte[rowLength];

    this.currentSize = currentSize;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#add(java.lang.Object)
   */
  @Override
  public boolean add(byte[] byteData) {
    if (this.currentSize >= this.rowCount) {
      throw new RuntimeException("PagedArrayList capacity exceeded");
    }

    if (byteData.length > this.rowLength) {
      throw new RuntimeException("Length of data is larger than capacity");
    }

    this.byteBuffer.position(this.currentSize * this.rowLength);
    this.byteBuffer.put(byteData);
    this.byteBuffer.put(this.empty, 0, this.rowLength - byteData.length);
    this.currentSize++;
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#add(int, java.lang.Object)
   */
  @Override
  public void add(int index, byte[] element) {
    throw new UnsupportedOperationException("Insert operations are not allowed on a file backed array");

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#addAll(java.util.Collection)
   */
  @Override
  public boolean addAll(Collection<? extends byte[]> c) {
    for (byte[] s : c) {
      this.add(s);
    }
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#addAll(int, java.util.Collection)
   */
  @Override
  public boolean addAll(int index, Collection<? extends byte[]> c) {
    throw new UnsupportedOperationException("Insert operations are not allowed on a file backed array");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#clear()
   */
  @Override
  public void clear() {
    throw new UnsupportedOperationException("Clear operations are not allowed on a file backed array");

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#contains(java.lang.Object)
   */
  @Override
  public boolean contains(Object o) {
    return this.indexOf(o) >= 0;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#containsAll(java.util.Collection)
   */
  @Override
  public boolean containsAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  /**
   * Forces the byte buffer to disk, must be called before closing the file
   */
  public void forceToDisk() {
    this.byteBuffer.force();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#get(int)
   */
  @Override
  public byte[] get(int index) {
    if (index > this.currentSize) {
      throw new IndexOutOfBoundsException("Index is out of bounds:" + index + "," + this.currentSize);
    }
    byte[] retBuffer = new byte[this.rowLength];
    int idx = index * this.rowLength;
    this.byteBuffer.position(idx);
    this.byteBuffer.get(retBuffer, 0, this.rowLength);
    return retBuffer;
  }

  /**
   * Gets the length of each row in this PagedArrayList
   * 
   * @return int of the row length
   */
  public int getLineLength() {
    return this.rowLength;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#indexOf(java.lang.Object)
   */
  @Override
  public int indexOf(Object o) {
    if (o == null) {
      return -1;
    }
    else {
      for (int i = 0; i < this.size(); i++) {
        if (o.equals(this.get(i))) {
          return i;
        }
      }
    }
    return -1;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#isEmpty()
   */
  @Override
  public boolean isEmpty() {
    if (this.currentSize == 0) {
      return true;
    }
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#iterator()
   */
  @Override
  public Iterator<byte[]> iterator() {
    return new PagedArrayListIterator(this);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#lastIndexOf(java.lang.Object)
   */
  @Override
  public int lastIndexOf(Object o) {
    if (o == null) {
      return -1;
    }
    else {
      for (int i = this.size() - 1; i >= 0; i--) {
        if (o.equals(this.get(i))) {
          return i;
        }
      }
    }
    return -1;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#listIterator()
   */
  @Override
  public ListIterator<byte[]> listIterator() {
    return new PagedArrayListIterator(this);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#listIterator(int)
   */
  @Override
  public ListIterator<byte[]> listIterator(int index) {
    return new PagedArrayListIterator(this, index);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#remove(int)
   */
  @Override
  public byte[] remove(int index) {
    throw new UnsupportedOperationException("Remove is not allowed on a file backed array");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#remove(java.lang.Object)
   */
  @Override
  public boolean remove(Object o) {
    throw new UnsupportedOperationException("Remove is not allowed on a file backed array");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#removeAll(java.util.Collection)
   */
  @Override
  public boolean removeAll(Collection<?> c) {
    throw new UnsupportedOperationException("Remove is not allowed on a file backed array");
  }

  /**
   * Resizes the row length to a new longer length in a new MappedByteBuffer
   * 
   * @param newRowLength
   *          int of the new row length
   * 
   * @param newBuffer
   *          MappedByteBuffer to write the new data to
   */
  public void resizeRowLength(int newRowLength, MappedByteBuffer newBuffer) {
    for (int i = this.size() - 1; i >= 0; i--) {
      this.setInBuffer(i, newRowLength, this.get(i), newBuffer);
    }
    this.rowLength = newRowLength;
    this.byteBuffer = newBuffer;
    this.empty = new byte[this.rowLength];

  }

  /**
   * Resizes the row length to a new shorter length, truncating as required in a new MappedByteBuffer
   * 
   * @param newRowLength
   *          int of the new row length
   * @param newBuffer
   *          MappedByteBuffer to write the new data to
   */
  public void resizeRowLengthTruncate(int newRowLength, MappedByteBuffer newBuffer) {
    for (int i = 0; i < this.size(); i++) {
      this.setInBuffer(i, newRowLength, this.get(i), newBuffer, true);
    }
    this.rowLength = newRowLength;
    this.byteBuffer = newBuffer;
    this.empty = new byte[this.rowLength];

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#retainAll(java.util.Collection)
   */
  @Override
  public boolean retainAll(Collection<?> c) {
    throw new UnsupportedOperationException("Remove is not allowed on a file backed array");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#set(int, java.lang.Object)
   */
  @Override
  public byte[] set(int index, byte[] element) {
    byte[] original = this.get(index);
    if (element.length > this.rowLength) {
      throw new RuntimeException("Length of data is larger than capacity");
    }

    this.byteBuffer.position(index * this.rowLength);
    this.byteBuffer.put(element);
    this.byteBuffer.put(this.empty, 0, this.rowLength - element.length);

    return original;

  }

  /**
   * Sets an element within the specified MappedByteBuffer
   * 
   * @param index
   *          int index of item to be set
   * @param bufferRowLength
   *          int of the row length
   * @param element
   *          byte array containing element to be set
   * @param newBuffer
   *          MappedByteBuffer to store value in
   */
  private void setInBuffer(int index, int bufferRowLength, byte[] element, MappedByteBuffer newBuffer) {
    this.setInBuffer(index, bufferRowLength, element, newBuffer, false);
  }

  /**
   * Sets an element within the specified MappedByteBuffer
   * 
   * @param index
   *          int index of item to be set
   * @param bufferRowLength
   *          int of the row length
   * @param element
   *          byte array containing element to be set
   * @param newBuffer
   *          MappedByteBuffer to store value in
   * @param truncate
   *          boolean true indicates truncate values if too long
   */
  private void setInBuffer(int index, int bufferRowLength, byte[] element, MappedByteBuffer newBuffer, boolean truncate) {
    if (!truncate && element.length > bufferRowLength) {
      throw new RuntimeException("Length of data is larger than capacity");
    }

    newBuffer.position(index * bufferRowLength);
    if (truncate) {
      newBuffer.put(element, 0, bufferRowLength);
      if (element.length < bufferRowLength) {
        newBuffer.put(this.empty, 0, bufferRowLength - element.length);
      }
    }
    else {
      newBuffer.put(element);
      newBuffer.put(this.empty, 0, bufferRowLength - element.length);
    }

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#size()
   */
  @Override
  public int size() {
    return this.currentSize;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#subList(int, int)
   */
  @Override
  public List<byte[]> subList(int fromIndex, int toIndex) {
    throw new UnsupportedOperationException("Sublist are not supported for file backed arrays");
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#toArray()
   */
  @Override
  public Object[] toArray() {

    byte[][] outputArray = new byte[this.size()][this.rowLength];
    for (int i = 0; i < this.size(); i++) {
      outputArray[i] = this.get(i);
    }
    return outputArray;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.List#toArray(java.lang.Object[])
   */
  @SuppressWarnings("unchecked")
  @Override
  public <T> T[] toArray(T[] a) {
    if (a.length < this.size()) {
      a = Arrays.copyOf(a, this.size());
    }
    for (int i = 0; i < this.size(); i++) {
      a[i] = (T) this.get(i);
    }
    return a;
  }
}
