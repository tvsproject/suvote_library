/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.collections;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * List iterator for a PagedArray. This provides a way of iterating across a single PagedArray. PagedArrays representing their data
 * as byte arrays, because the data is being written to file, hence all the operations performed at this level are byte array based.
 * 
 * @author Chris Culnane
 * 
 */
public class PagedArrayListIterator implements Iterator<byte[]>, ListIterator<byte[]> {

  /**
   * byte array containing current item, this is an efficiency step to avoid having to load the entire paged array into memory when
   * comparing single elements. It is particularly useful during merge sort operations where pages are compared
   */
  private byte[]       current;

  /**
   * int containing initial start index
   */
  private int          index = -1;

  /**
   * List of byte array representations of the underling paged array
   */
  private List<byte[]> list;

  /**
   * Constructs a new PagedArrayListIterator for a list of byte arrays
   * 
   * @param list
   *          List of byte arrays
   */
  public PagedArrayListIterator(List<byte[]> list) {
    this.list = list;
  }

  /**
   * Constructs a new PagedArrayListIterator for a list of byte arrays starting at the index specified
   * 
   * @param list
   *          List of byte arrays
   * @param startIndex
   *          int of start index
   */
  public PagedArrayListIterator(List<byte[]> list, int startIndex) {
    this.list = list;
    this.index = startIndex;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#add(java.lang.Object)
   */
  @Override
  public void add(byte[] e) {
    this.list.add(e);
  }

  /**
   * Additional method to standard iterators, this provides access to the current (last retrieved) element without having to reload
   * the entire page into memory. This has no impact on the position of the iterator,
   * 
   * @return byte array of the current (last retrieved) element
   */
  public byte[] current() {
    return this.current;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Iterator#hasNext()
   */
  @Override
  public boolean hasNext() {
    if (this.list.size() > (this.index + 1)) {
      return true;
    }
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#hasPrevious()
   */
  @Override
  public boolean hasPrevious() {
    if (this.index > 0) {
      return true;
    }
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Iterator#next()
   */
  @Override
  public byte[] next() {
    this.index++;
    this.current = this.list.get(this.index);

    return this.list.get(this.index);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#nextIndex()
   */
  @Override
  public int nextIndex() {
    return this.index + 1;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#previous()
   */
  @Override
  public byte[] previous() {
    return this.list.get(this.index - 1);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#previousIndex()
   */
  @Override
  public int previousIndex() {
    return this.index - 1;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Iterator#remove()
   */
  @Override
  public void remove() {
    throw new UnsupportedOperationException();

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#set(java.lang.Object)
   */
  @Override
  public void set(byte[] e) {
    this.list.set(this.index, e);
  }

}
