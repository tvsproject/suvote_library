/*******************************************************************************
 * Copyright (c) 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.collections;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Iterator for MappedFileArrayList of type <A>
 * 
 * @author Chris Culnane
 * 
 */
public class MappedFileArrayListIterator<A> implements Iterator<A>, ListIterator<A> {

  /**
   * index, initially set to -1
   */
  private int     index = -1;
  /**
   * References for list
   */
  private List<A> list;

  /**
   * Constructs a new MappedFileArrayListIterator for a list of type <A>
   * 
   * @param list
   *          List of type <A>
   */
  public MappedFileArrayListIterator(List<A> list) {
    this.list = list;
  }

  /**
   * Constructs a new MappedFileArrayListIterator for a list of type <A> starting at the specified index.
   * 
   * @param list
   *          list List of type <A>
   * @param startIndex
   *          int of index to start at
   */
  public MappedFileArrayListIterator(List<A> list, int startIndex) {
    this.list = list;
    this.index = startIndex;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#add(java.lang.Object)
   */
  @Override
  public void add(A e) {
    this.list.add(e);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Iterator#hasNext()
   */
  @Override
  public boolean hasNext() {
    if (this.list.size() > (this.index + 1)) {
      return true;
    }
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#hasPrevious()
   */
  @Override
  public boolean hasPrevious() {
    if (this.index > 0) {
      return true;
    }
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Iterator#next()
   */
  @Override
  public A next() {
    this.index++;
    return this.list.get(this.index);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#nextIndex()
   */
  @Override
  public int nextIndex() {
    return this.index + 1;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#previous()
   */
  @Override
  public A previous() {
    return this.list.get(this.index - 1);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#previousIndex()
   */
  @Override
  public int previousIndex() {
    return this.index - 1;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Iterator#remove()
   */
  @Override
  public void remove() {
    throw new UnsupportedOperationException();

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.ListIterator#set(java.lang.Object)
   */
  @Override
  public void set(A e) {
    this.list.set(this.index, e);
  }

}
