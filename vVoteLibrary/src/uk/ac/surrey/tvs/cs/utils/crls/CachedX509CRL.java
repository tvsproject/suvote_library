/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.tvs.cs.utils.crls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.util.Date;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.tvs.cs.utils.fswatcher.FileChangeListener;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileSystemWatcher;

/**
 * An extension of the X509CRL object that provides a wrapper for setting up and listening to FileChange events that are fired by
 * the FileSystemWatcher. This allows the build SSLServerSocket, and other such classes, to seamlessly use this class with the
 * automatic updating being transparent them.
 * 
 * @author Chris Culnane
 * 
 */
public class CachedX509CRL extends X509CRL implements FileChangeListener {

  /**
   * Volatile X509CRL this is a reference to the most recent instance of the CRL
   */
  private volatile X509CRL    cachedCRL;

  /**
   * The File that contains the crlFile and the file we will monitor for changes
   */
  private File                crlFile;

  /**
   * Reference to the certificate factory to use for loading the actual CRL
   */
  private CertificateFactory  certFactory;
  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(CachedX509CRL.class);

  /**
   * Constructs a new CachedX509CRL from the specified file in filePath and using the certFactory. Because no FileSystemWatcher is
   * specified this is a static CRL that will not update when changes are made to the underlying file.
   * 
   * @param filePath
   *          File that contains the CRL to load
   * @param certFactory
   *          CertificateFactory to use when loading the CRL specified in filePath
   * @throws CachedX509CRLException
   */
  public CachedX509CRL(File filePath, CertificateFactory certFactory) throws CachedX509CRLException {
    this.certFactory = certFactory;
    logger.info("Loading CRL from {}", filePath);
    this.crlFile = filePath;
    this.cachedCRL = this.loadCRLFromFile(this.crlFile);
  }

  /**
   * Constructs a new CachedX509CRL, from the specified file in filePath and listens for any file system changes to the file via the
   * FileSystemWatcher in watched. This will automatically update if the file changes on the file system and is the intended usage
   * of this class.
   * 
   * @param filePath
   *          File that contains the CRL to load
   * @param certFactory
   *          CertificateFactory to use when loading the CRL specified in filePath
   * @param watcher
   *          FileSystemWatcher to use for monitoring file change events
   * @throws CachedX509CRLException
   */
  public CachedX509CRL(File filePath, CertificateFactory certFactory, FileSystemWatcher watcher) throws CachedX509CRLException {
    this(filePath, certFactory);
    try {
      watcher.addFileListener(this.crlFile, this);
    }
    catch (IOException e) {
      throw new CachedX509CRLException("IOException whilst loading CRLFile", e);
    }
  }

  /**
   * Constructs a new CachedX509CRL from the specified crl located at the path in filePath and using the certFactory. Because no FileSystemWatcher is
   * specified this is a static CRL that will not update when changes are made to the underlying file.
   * 
   * @param filePath
   *          String that contains the path to CRL to load
   * @param certFactory
   *          CertificateFactory to use when loading the CRL specified in filePath
   * @throws CachedX509CRLException
   */
  public CachedX509CRL(String filePath, CertificateFactory certFactory) throws CachedX509CRLException {
    this(new File(filePath), certFactory);
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.tvs.cs.utils.crls.FileChangeListener#fileChanged(java.io.File)
   */
  @Override
  public void fileChanged(File filePath) {
    X509CRL newCRL;
    try {
      newCRL = this.loadCRLFromFile(filePath);
      this.cachedCRL = newCRL;
      this.crlFile = filePath;
      logger.info("{} has changed, have updated cache", filePath.getAbsolutePath());
    }
    catch (CachedX509CRLException e) {
      logger.error("Error loading CRL from file. Will not update cache", e);
    }

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509Extension#getCriticalExtensionOIDs()
   */
  @Override
  public Set<String> getCriticalExtensionOIDs() {
    return this.cachedCRL.getCriticalExtensionOIDs();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getEncoded()
   */
  @Override
  public byte[] getEncoded() throws CRLException {
    return this.cachedCRL.getEncoded();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509Extension#getExtensionValue(java.lang.String)
   */
  @Override
  public byte[] getExtensionValue(String oid) {
    return this.cachedCRL.getExtensionValue(oid);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getIssuerDN()
   */
  @Override
  public Principal getIssuerDN() {
    return this.cachedCRL.getIssuerDN();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getNextUpdate()
   */
  @Override
  public Date getNextUpdate() {
    return this.cachedCRL.getNextUpdate();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509Extension#getNonCriticalExtensionOIDs()
   */
  @Override
  public Set<String> getNonCriticalExtensionOIDs() {
    return this.cachedCRL.getNonCriticalExtensionOIDs();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getRevokedCertificate(java.math.BigInteger)
   */
  @Override
  public X509CRLEntry getRevokedCertificate(BigInteger serialNumber) {
    return this.cachedCRL.getRevokedCertificate(serialNumber);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getRevokedCertificates()
   */
  @Override
  public Set<? extends X509CRLEntry> getRevokedCertificates() {
    return this.cachedCRL.getRevokedCertificates();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getSigAlgName()
   */
  @Override
  public String getSigAlgName() {
    return this.cachedCRL.getSigAlgName();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getSigAlgOID()
   */
  @Override
  public String getSigAlgOID() {
    return this.cachedCRL.getSigAlgOID();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getSigAlgParams()
   */
  @Override
  public byte[] getSigAlgParams() {
    return this.cachedCRL.getSigAlgParams();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getSignature()
   */
  @Override
  public byte[] getSignature() {
    return this.cachedCRL.getSignature();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getTBSCertList()
   */
  @Override
  public byte[] getTBSCertList() throws CRLException {
    return this.cachedCRL.getTBSCertList();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getThisUpdate()
   */
  @Override
  public Date getThisUpdate() {
    return this.cachedCRL.getThisUpdate();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#getVersion()
   */
  @Override
  public int getVersion() {
    return this.cachedCRL.getVersion();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509Extension#hasUnsupportedCriticalExtension()
   */
  @Override
  public boolean hasUnsupportedCriticalExtension() {
    return this.cachedCRL.hasUnsupportedCriticalExtension();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.CRL#isRevoked(java.security.cert.Certificate)
   */
  @Override
  public boolean isRevoked(Certificate cert) {

    return this.cachedCRL.isRevoked(cert);
  }

  
  /**
   * Utility method for loading a CRL from a file. Checks the file exists and then loads it via the CertifcateFactory before casting it to an appropriate type. 
   * @param crlToLoad File that contains the CRL that needs to be loaded
   * @return X509CRL loaded from the specified file
   * @throws CachedX509CRLException
   */
  private X509CRL loadCRLFromFile(File crlToLoad) throws CachedX509CRLException {
    if (!crlToLoad.exists()) {
      throw new CachedX509CRLException("File path does not exist");
    }
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(crlToLoad);
      return (X509CRL) this.certFactory.generateCRL(fis);
    }
    catch (FileNotFoundException e) {
      throw new CachedX509CRLException("File not found", e);
    }
    catch (CRLException e) {
      throw new CachedX509CRLException("CRL Exception when loading CRL", e);
    }
    finally {
      if (fis != null) {
        try {
          fis.close();
        }
        catch (IOException e) {
          logger.warn("Exception closing IO Stream", e);
        }
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.CRL#toString()
   */
  @Override
  public String toString() {
    return this.cachedCRL.toString();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#verify(java.security.PublicKey)
   */
  @Override
  public void verify(PublicKey key) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException,
      SignatureException {
    this.cachedCRL.verify(key);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.security.cert.X509CRL#verify(java.security.PublicKey, java.lang.String)
   */
  @Override
  public void verify(PublicKey key, String sigProvider) throws CRLException, NoSuchAlgorithmException, InvalidKeyException,
      NoSuchProviderException, SignatureException {
    this.cachedCRL.verify(key, sigProvider);
  }

}
