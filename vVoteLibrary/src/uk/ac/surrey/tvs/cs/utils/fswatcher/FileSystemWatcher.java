/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.tvs.cs.utils.fswatcher;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FileSystemWatcher uses the WatchService to monitor a specified folder and capture any file change events on the contained files.
 * It provides a listener interface to allow interested objects to be notified of changes to particular files in that folder without
 * having to individually monitor them. This is derived from the example at
 * http://docs.oracle.com/javase/tutorial/essential/io/notification.html
 * 
 * TODO Check thread safety - not urgent since we initialise on a single thread, but should be done nonetheless
 * 
 * @author Chris Culnane
 * 
 */
public class FileSystemWatcher implements Runnable {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(FileSystemWatcher.class);

  /**
   * Static cast method for casting the WatchEvent
   * 
   * @param event
   *          WatchEvent that has occurred
   * @return WatchEvent<T> event that has occurred
   */
  @SuppressWarnings("unchecked")
  static <T> WatchEvent<T> cast(WatchEvent<?> event) {
    return (WatchEvent<T>) event;
  }

  /**
   * ArrayList of FolderChangeListeners that have registered to listen for any changes that occur in the folder
   */
  private ArrayList<FolderChangeListener>                listeners     = new ArrayList<FolderChangeListener>();

  /**
   * HashMap of files, and their respective FileChangeListeners, that have registered as wanting to listen for changes to the
   * specified file
   */
  private HashMap<String, ArrayList<FileChangeListener>> fileListeners = new HashMap<String, ArrayList<FileChangeListener>>();

  /**
   * ExecutorService to run the FileSystemWatcher in
   */
  private ExecutorService                                executor;

  /**
   * Underlying WatchService that performs the native IO file watching
   */
  private WatchService                                   watcher;

  /**
   * Java NIO Path to the directory to watch
   */
  private Path                                           dir;

  /**
   * File that contains the directory path to watch
   */
  private File                                           dirPath;

  /**
   * WatchKey that holds the reference to the native watcher for the folder
   */
  private WatchKey                                       watchKey;

  /**
   * Boolean that records whether the FileSystemWatcher has started watching or not
   */
  private boolean                                        watching      = false;

  /**
   * Constructs a new FileSystemWatcher for the specified directory
   * 
   * @param dirPath
   *          File that holds a reference to the directory to watch
   * @throws FileWatcherException
   */
  public FileSystemWatcher(File dirPath) throws FileWatcherException {
    this.dirPath = dirPath;
  }

  /**
   * Constructs a new FileSystemWatcher for the specified directory
   * 
   * @param dirPath
   *          File that holds a reference to the directory to watch
   * @throws FileWatcherException
   */
  public FileSystemWatcher(String dirPath) throws FileWatcherException {
    this.dirPath = new File(dirPath);
  }

  /**
   * Adds a file listener to the watcher that will receive file change events for the specified file.
   * 
   * @param file
   *          File that the listener wants to receive file change events on
   * @param listener
   *          FileChangeListener to call when an event occurs
   * @throws IOException
   */
  public synchronized void addFileListener(File file, FileChangeListener listener) throws IOException {
    if (this.fileListeners.containsKey(file.getCanonicalPath())) {
      this.fileListeners.get(file.getCanonicalPath()).add(listener);
    }
    else {
      ArrayList<FileChangeListener> listenerArray = new ArrayList<FileChangeListener>();
      listenerArray.add(listener);
      this.fileListeners.put(file.getCanonicalPath(), listenerArray);
    }
  }

  /**
   * Adds a folder change listener that will receive notifications of any change that takes place in the folder.
   * 
   * @param listener
   *          FolderChangeListener to receive change events
   */
  public synchronized void addListener(FolderChangeListener listener) {
    logger.info("Adding listener to FolderWatcher[{}]", this.dirPath);
    this.listeners.add(listener);
  }

  /**
   * Converts the WatchEvent.Kind into a FileChangeType for internal use
   * 
   * @param kind
   *          WatchEvent.Kind to be converted
   * @return FileChangeType equivalent to the WatchEvent.Kind
   */
  private FileChangeType convertType(WatchEvent.Kind<?> kind) {

    if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
      return FileChangeType.CREATED;
    }
    else if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
      return FileChangeType.DELETED;
    }
    else if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
      return FileChangeType.MODIFIED;
    }
    else {
      return FileChangeType.UNKNOWN;
    }
  }

  /**
   * Fires both a folder change event and any specified file change events on any interested listeners. The folder wide event is
   * fired first before checking whether any listeners are registered to receive individual file change events for this specific
   * file.
   * 
   * @param changedFile
   *          Path that refers to the file in the folder that has changed
   * @param type
   *          FileChangeType that signifies the type of change
   */
  private synchronized void fireChangeEvent(Path changedFile, FileChangeType type) {

    File file = changedFile.toFile();
    for (FolderChangeListener listener : this.listeners) {
      listener.folderChanged(file.getAbsoluteFile(), type);
    }
    if (type == FileChangeType.MODIFIED || type == FileChangeType.CREATED) {
      try {
        if (this.fileListeners.containsKey(file.getCanonicalPath())) {
          this.fireFileChangeEvent(file);
        }
      }
      catch (IOException e) {
        logger.warn("IOException getting Canonical Path, will try to continue", e);
      }

    }

  }

  /**
   * Fires the actual individual file change event by looking up any listeners that exist for the file and notifying them
   * 
   * @param changedFile
   *          File that points to the changed file
   */
  private synchronized void fireFileChangeEvent(File changedFile) {
    try {
      ArrayList<FileChangeListener> listenerArr = this.fileListeners.get(changedFile.getCanonicalPath());
      if (listenerArr != null) {
        for (FileChangeListener listener : listenerArr) {
          listener.fileChanged(changedFile.getAbsoluteFile());
        }
      }
    }
    catch (IOException e) {
      logger.warn("IOException when getting canonical path, will try to continue", e);
    }
  }

  /**
   * Performs the necessary preparation and initialisation prior to the start of watching. This constructs a new watch service and
   * prepares an ExecutorService, with a daemon thread factory, to run the FileSystemWatcher in. However, this does not actually
   * start the file system watching. That only occurs when startWatching is called
   * 
   * @throws FileWatcherException
   */
  private synchronized void prepareFolderWatcher() throws FileWatcherException {

    this.watching = true;
    try {
      this.watcher = FileSystems.getDefault().newWatchService();
    }
    catch (IOException e) {
      throw new FileWatcherException("Exception creating watch service", e);
    }
    logger.info("Preparing folder watcher for {}", this.dirPath);
    this.dir = this.dirPath.toPath();
    logger.info("Preparing new executor");
    this.executor = Executors.newSingleThreadExecutor(new DaemonThreadFactory("FileWatcher"));
    try {
      this.watchKey = this.dir.register(this.watcher, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE,
          StandardWatchEventKinds.ENTRY_DELETE);
      logger.info("Registered watch key");
    }
    catch (IOException e) {
      throw new FileWatcherException("Exception registering watcher", e);
    }

  }

  /**
   * Removes a specified FileChangeListener from list of registered listens on files.
   * 
   * @param file
   *          File that contains a reference to the file that was being monitored
   * @param listener
   *          FileChangeListener to remove from that files list of listeners
   * @throws IOException
   */
  public synchronized void removeFileListener(File file, FileChangeListener listener) throws IOException {
    if (this.fileListeners.containsKey(file.getCanonicalPath())) {
      this.fileListeners.get(file.getCanonicalPath()).remove(listener);
      if (this.fileListeners.get(file.getCanonicalPath()).isEmpty()) {
        this.fileListeners.remove(file.getCanonicalPath());
      }
    }
  }

  /**
   * Removes a FolderChangeListener from the list of listeners
   * 
   * @param listener
   *          FolderChangeListener to remove
   */
  public synchronized void removeListener(FolderChangeListener listener) {
    logger.info("Removing listener from FolderWatcher[{}]", this.dirPath);
    this.listeners.remove(listener);
  }

  /**
   * method that awaits file change events
   */
  @Override
  public void run() {

    for (;;) {

      // wait for key to be signaled
      WatchKey key;
      try {
        synchronized (this.watcher) {
          key = this.watcher.take();
        }
        logger.info("Watcher returned key");
      }
      catch (InterruptedException e) {
        logger.warn("FileWatcher received InterruptedException", e);
        return;
      }

      for (WatchEvent<?> event : key.pollEvents()) {
        WatchEvent.Kind<?> kind = event.kind();

        // This key is registered only for ENTRY_CREATE events,
        // but an OVERFLOW event can occur regardless if events are
        // lost or discarded.
        if (kind == StandardWatchEventKinds.OVERFLOW) {
          continue;
        }

        // The filename is the context of the event.
        WatchEvent<Path> ev = cast(event);

        Path filename = ev.context();
        logger.info("File changed event occurred on {}", filename.toString());
        synchronized (this) {
          this.fireChangeEvent(this.dir.resolve(filename), this.convertType(kind));
        }

      }

      // Reset the key -- this step is critical if you want to receive
      // further watch events. If the key is no longer valid, the directory
      // is inaccessible so exit the loop.
      boolean valid = key.reset();
      if (!valid) {
        break;
      }
    }
  }

  /**
   * Starts the watching of the folded. If the FileSystemWatcher is already watching it will initially stop it, prepare it again and
   * then start it.
   * 
   * @throws FileWatcherException
   */
  public synchronized void startWatching() throws FileWatcherException {
    if (this.watching) {
      this.stopWatching();
    }
    this.prepareFolderWatcher();
    logger.info("Starting FileWatcher for: {}", this.dirPath);
    this.executor.execute(this);
  }

  /**
   * Stops the FileSystemWatcher and tidies up any underlying references to NIO WatchKeys
   * 
   * @throws FileWatcherException
   */
  public synchronized void stopWatching() throws FileWatcherException {
    logger.info("Stopping FileWatcher for: {}", this.dirPath);
    this.executor.shutdownNow();
    if (this.watchKey.isValid()) {
      this.watchKey.cancel();
    }
    logger.info("Closing watch service");
    try {
      this.watcher.close();
    }
    catch (IOException e) {
      throw new FileWatcherException("Exception closing down watcher service", e);
    }
    this.watching = false;
  }
}
