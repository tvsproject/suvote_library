/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>UploadedZipFileEntryTooBigExceptionTest</code> contains tests for the class
 * <code>{@link UploadedZipFileEntryTooBigException}</code>.
 */
public class UploadedZipFileEntryTooBigExceptionTest {

  /**
   * Run the UploadedZipFileEntryTooBigException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileEntryTooBigException_1() throws Exception {
    UploadedZipFileEntryTooBigException result = new UploadedZipFileEntryTooBigException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the UploadedZipFileEntryTooBigException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileEntryTooBigException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    UploadedZipFileEntryTooBigException result = new UploadedZipFileEntryTooBigException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException: "
        + LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the UploadedZipFileEntryTooBigException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileEntryTooBigException_3() throws Exception {
    Throwable cause = new Throwable();

    UploadedZipFileEntryTooBigException result = new UploadedZipFileEntryTooBigException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException: java.lang.Throwable",
        result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the UploadedZipFileEntryTooBigException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileEntryTooBigException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    UploadedZipFileEntryTooBigException result = new UploadedZipFileEntryTooBigException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException: "
        + LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the UploadedZipFileEntryTooBigException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileEntryTooBigException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    UploadedZipFileEntryTooBigException result = new UploadedZipFileEntryTooBigException(message, cause, enableSuppression,
        writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException: "
        + LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}