/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;

/**
 * The class <code>TimeoutManagerTest</code> contains tests for the class <code>{@link TimeoutManager}</code>.
 */
public class TimeoutManagerTest {

  /**
   * Dummy timeout runnable.
   */
  private class DummyTimeout implements Runnable {

    /** Has the timeout been called? */
    private boolean timeout = false;

    /**
     * @return the timeout.
     */
    private boolean isTimeout() {
      return this.timeout;
    }

    /**
     * Executed when the timeout occurs.
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
      this.timeout = true;
    }
  }

  /**
   * Run the void addDefaultTimeout(Runnable) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddDefaultTimeout_1() throws Exception {
    TimeoutManager fixture = new TimeoutManager(LibraryTestParameters.SLEEP_INTERVAL, LibraryTestParameters.SUBMISSION_TIMEOUT,
        LibraryTestParameters.SUBMISSION_TIMEOUT);

    // Put the TimeoutManager in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Now add a default timeout task.
    DummyTimeout timeoutTask = new DummyTimeout();
    fixture.addDefaultTimeout(timeoutTask);
    assertFalse(timeoutTask.isTimeout());

    // Wait for the timeout period to have taken place.
    int count = 0;

    while (!timeoutTask.isTimeout()) {
      try {
        // Sleep for the interval.
        Thread.sleep(LibraryTestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }

      count++;
    }

    assertTrue(count >= LibraryTestParameters.SUBMISSION_TIMEOUT);
    assertTrue(count <= LibraryTestParameters.SUBMISSION_TIMEOUT + 2); // Wriggle room.

    // Shutdown.
    fixture.shutdown();

    // Wait for the thread to finish.
    thread.join();
    assertFalse(thread.isAlive());
  }

  /**
   * Run the void addTimeout(Runnable,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddTimeout_1() throws Exception {
    TimeoutManager fixture = new TimeoutManager(LibraryTestParameters.SLEEP_INTERVAL, LibraryTestParameters.SUBMISSION_TIMEOUT,
        LibraryTestParameters.SUBMISSION_TIMEOUT);

    // Put the TimeoutManager in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Now add a default timeout task.
    DummyTimeout timeoutTask = new DummyTimeout();
    fixture.addTimeout(timeoutTask, LibraryTestParameters.SHORT_SUBMISSION_TIMEOUT);
    assertFalse(timeoutTask.isTimeout());

    // Wait for the timeout period to have taken place.
    int count = 0;

    while (!timeoutTask.isTimeout()) {
      try {
        // Sleep for the interval.
        Thread.sleep(LibraryTestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }

      count++;
    }

    assertTrue(count >= LibraryTestParameters.SHORT_SUBMISSION_TIMEOUT);
    assertTrue(count <= LibraryTestParameters.SHORT_SUBMISSION_TIMEOUT + 2); // Wriggle room.

    // Shutdown.
    fixture.shutdown();

    // Wait for the thread to finish.
    thread.join();
    assertFalse(thread.isAlive());
  }

  /**
   * Run the void addTimeout(Runnable,int) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MaxTimeoutExceeded.class)
  public void testAddTimeout_2() throws Exception {
    TimeoutManager fixture = new TimeoutManager(LibraryTestParameters.SLEEP_INTERVAL, LibraryTestParameters.SUBMISSION_TIMEOUT,
        LibraryTestParameters.SUBMISSION_TIMEOUT);

    // Now add a default timeout task.
    DummyTimeout timeoutTask = new DummyTimeout();
    fixture.addTimeout(timeoutTask, LibraryTestParameters.SUBMISSION_TIMEOUT * 4);
  }

  /**
   * Run the TimeoutManager(int,int,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTimeoutManager_1() throws Exception {
    TimeoutManager result = new TimeoutManager(LibraryTestParameters.SLEEP_INTERVAL, LibraryTestParameters.SUBMISSION_TIMEOUT,
        LibraryTestParameters.SUBMISSION_TIMEOUT);
    assertNotNull(result);
  }

  /**
   * Run the TimeoutManager(int,int,int) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MaxTimeoutExceeded.class)
  public void testTimeoutManager_2() throws Exception {
    TimeoutManager result = new TimeoutManager(LibraryTestParameters.SLEEP_INTERVAL, LibraryTestParameters.SUBMISSION_TIMEOUT,
        LibraryTestParameters.SUBMISSION_TIMEOUT * 2);
    assertNull(result);
  }
}