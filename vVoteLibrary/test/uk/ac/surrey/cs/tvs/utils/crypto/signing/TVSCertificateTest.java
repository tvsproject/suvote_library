/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPublicKey;

/**
 * @author james
 * 
 */
public class TVSCertificateTest {

	@Test
	public void testTVSCertificateCertificate() throws Exception {
		//read in test cert
		FileInputStream fis = new FileInputStream("testdata/SurreyPAV.pem");
		BufferedInputStream bis = new BufferedInputStream(fis);

		CertificateFactory cf = CertificateFactory.getInstance("X.509");

		Certificate cert = cf.generateCertificate(bis);
		bis.close();
		fis.close();
		
		//create new TVSCertificate using the test cert
		TVSCertificate tcert = new TVSCertificate(cert);
		
		assertNotNull(tcert);
		assertSame(cert,tcert.getCertificate());
	}

	@Test
	public void testTVSCertificateBLSPublicKey() throws Exception {
		
		//read in Peer 1's public key
		BufferedReader br = new BufferedReader(new FileReader("testdata/convertedKeys/Peer1_public.bks"));
		String json = "";
		while (br.ready())
			json += br.readLine();
		br.close();
		JSONObject jsonPubKey = new JSONObject(json);

		//the key itself is wrapped in a couple of extra layers
		JSONObject innerKey = jsonPubKey.getJSONObject("Peer1").getJSONObject("pubKeyEntry");

		BLSPublicKey pubkey = new BLSPublicKey(innerKey);
		
		assertNotNull(pubkey);
		assertNotNull(pubkey.getG());
		assertNotNull(pubkey.getPartialPublicKey());
		assertNotNull(pubkey.getPublicKey());
	}

}
