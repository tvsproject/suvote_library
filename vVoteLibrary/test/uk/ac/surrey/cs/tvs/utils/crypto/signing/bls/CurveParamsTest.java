package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import static org.junit.Assert.assertTrue;
import it.unisa.dia.gas.plaf.jpbc.pairing.DefaultCurveParameters;

import java.math.BigInteger;

import org.junit.Test;

public class CurveParamsTest {

	@Test
	public void testAll() throws Exception {
		CurveParams cparams = CurveParams.getInstance();
		DefaultCurveParameters dcparams = cparams.getCurveParams();
		//test the first parameter
		assertTrue(new BigInteger("625852803282871856053922297323874661378036491717").equals(dcparams.getBigInteger("q")));
		//test the biggest parameter
		assertTrue(new BigInteger("60094290356408407130984161127310078516360031868417968262992864809623507269833854678414046779817844853757026858774966331434198257512457993293271849043664655146443229029069463392046837830267994222789160047337432075266619082657640364986415435746294498140589844832666082434658532589211525696").equals(dcparams.getBigInteger("nk")));
		//test the last parameter
		assertTrue(new BigInteger("489829780399089087337095496766742416848202714236").equals(dcparams.getBigInteger("nqr")));
	}

}
