/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

/**
 * @author james
 *
 */
public class DefaultTimeoutFileFilterTest {

	@Test
	public void testDefaultTimeoutFileFilter() throws Exception {
		DefaultTimeoutFileFilter filter = new DefaultTimeoutFileFilter();
		assertNotNull(filter);
	}

	@Test
	public void testAccept() throws Exception {
		DefaultTimeoutFileFilter filter = new DefaultTimeoutFileFilter();
		
		//check a few obvious cases and corner cases
		assertTrue(filter.accept(new File("something.timeout")));
		assertTrue(filter.accept(new File("/usr/share/local/something.timeout")));
		assertTrue(filter.accept(new File("C:\\Windows\\something.timeout")));
		assertFalse(filter.accept(new File("pictureOfABunny.jpg")));
		assertFalse(filter.accept(new File("something.timeout.jpg")));
		assertTrue(filter.accept(new File("something.xls.timeout")));
	}

}
