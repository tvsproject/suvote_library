/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>CertificateRequestGenExceptionTest</code> contains tests for the class
 * <code>{@link CertificateRequestGenException}</code>.
 */
public class CertificateRequestGenExceptionTest {

  /**
   * Run the CertificateRequestGenException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateRequestGenException_1() throws Exception {

    CertificateRequestGenException result = new CertificateRequestGenException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the CertificateRequestGenException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateRequestGenException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    CertificateRequestGenException result = new CertificateRequestGenException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException: "
        + LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CertificateRequestGenException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateRequestGenException_3() throws Exception {
    Throwable cause = new Throwable();

    CertificateRequestGenException result = new CertificateRequestGenException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException: java.lang.Throwable",
        result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the CertificateRequestGenException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateRequestGenException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    CertificateRequestGenException result = new CertificateRequestGenException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException: "
        + LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CertificateRequestGenException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateRequestGenException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    CertificateRequestGenException result = new CertificateRequestGenException(message, cause, enableSuppression,
        writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException: "
        + LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}