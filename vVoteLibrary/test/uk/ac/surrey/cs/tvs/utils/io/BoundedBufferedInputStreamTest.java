/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException;

/**
 * The class <code>BoundedBufferedInputStreamTest</code> contains tests for the class
 * <code>{@link BoundedBufferedInputStream}</code>.
 */
public class BoundedBufferedInputStreamTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Run the BoundedBufferedInputStream(InputStream) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBoundedBufferedInputStream_1() throws Exception {
    String data = LibraryTestParameters.getData(LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length());

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream result = new BoundedBufferedInputStream(in);
    assertNotNull(result);

    result.close();
  }

  /**
   * Run the BoundedBufferedInputStream(InputStream) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBoundedBufferedInputStream_2() throws Exception {
    String data = LibraryTestParameters.getData(LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length());

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream result = new BoundedBufferedInputStream(in, LibraryTestParameters.FILE_LINE.length());
    assertNotNull(result);

    result.close();
  }

  /**
   * Run the BoundedBufferedInputStream(InputStream) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBoundedBufferedInputStream_3() throws Exception {
    String data = LibraryTestParameters.getData(LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length());

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream result = new BoundedBufferedInputStream(in, -1);
    assertNotNull(result);

    result.close();
  }

  /**
   * Run the BoundedBufferedInputStream(InputStream) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBoundedBufferedInputStream_4() throws Exception {
    String data = LibraryTestParameters.getData(LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length());

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream result = new BoundedBufferedInputStream(in, LibraryTestParameters.DATA_BOUND,
        LibraryTestParameters.FILE_LINE.length());
    assertNotNull(result);

    result.close();
  }

  /**
   * Run the BoundedBufferedInputStream(InputStream) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBoundedBufferedInputStream_5() throws Exception {
    String data = LibraryTestParameters.getData(LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length());

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream result = new BoundedBufferedInputStream(in, LibraryTestParameters.DATA_BOUND, -1);
    assertNotNull(result);

    result.close();
  }

  /**
   * Run the String readFile(long,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadFile_1() throws Exception {
    int lines = LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length();
    String data = LibraryTestParameters.getData(lines);

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream fixture = new BoundedBufferedInputStream(in);

    // Test reading the data and saving it to a file.
    File outputFile = new File(LibraryTestParameters.OUTPUT_DATA_FILE);
    String result = fixture.readFile(LibraryTestParameters.DATA_BOUND, outputFile);
    assertNotNull(result);

    fixture.close();

    // Test the data is correct.
    byte[] buffer = new byte[LibraryTestParameters.DATA_BOUND];
    in = new FileInputStream(outputFile);

    int length = in.read(buffer, 0, LibraryTestParameters.DATA_BOUND);
    assertEquals(LibraryTestParameters.DATA_BOUND, length);
    assertEquals(data, new String(buffer));

    in.close();

    // Test the digest.
    MessageDigest digest = MessageDigest.getInstance("SHA1");
    digest.update(data.getBytes());
    assertEquals(Base64.encodeBase64String(digest.digest()), result);
  }

  /**
   * Run the String readFile(long,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadFile_2() throws Exception {
    int lines = LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length();
    String data = LibraryTestParameters.getData(lines);

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream fixture = new BoundedBufferedInputStream(in);

    // Test reading the data and saving it to a file.
    File outputFile = new File(LibraryTestParameters.OUTPUT_DATA_FILE);
    String result = fixture.readFile(LibraryTestParameters.FILE_LINE.length(), outputFile);
    assertNotNull(result);

    fixture.close();

    // Test the data is correct.
    byte[] buffer = new byte[LibraryTestParameters.FILE_LINE.length()];
    in = new FileInputStream(outputFile);

    int length = in.read(buffer, 0, LibraryTestParameters.FILE_LINE.length());
    assertEquals(LibraryTestParameters.FILE_LINE.length(), length);
    assertEquals(LibraryTestParameters.FILE_LINE, new String(buffer));

    in.close();

    // Test the digest.
    MessageDigest digest = MessageDigest.getInstance("SHA1");
    digest.update(LibraryTestParameters.FILE_LINE.getBytes());
    assertEquals(Base64.encodeBase64String(digest.digest()), result);
  }

  /**
   * Run the String readFile(long,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadFile_3() throws Exception {
    int lines = LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length();
    String data = LibraryTestParameters.getData(lines);

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream fixture = new BoundedBufferedInputStream(in);

    // Test reading the data and saving it to a file.
    File outputFile = new File(LibraryTestParameters.OUTPUT_DATA_FILE);
    String result = fixture.readFile(0, outputFile);
    assertNotNull(result);

    fixture.close();

    // Test the data is correct.
    byte[] buffer = new byte[LibraryTestParameters.DATA_BOUND];
    in = new FileInputStream(outputFile);

    int length = in.read(buffer, 0, LibraryTestParameters.DATA_BOUND);
    assertEquals(-1, length);

    in.close();
  }

  /**
   * Run the String readLine() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadLine_1() throws Exception {
    int lines = LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length();
    String data = LibraryTestParameters.getData(lines);

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream fixture = new BoundedBufferedInputStream(in);

    // Test reading lines.
    for (int i = 0; i < lines; i++) {
      String result = fixture.readLine();
      assertNotNull(result);
      assertEquals(LibraryTestParameters.FILE_LINE, result + "\n");
    }

    fixture.close();
  }

  /**
   * Run the String readLine() method test.
   * 
   * @throws Exception
   */
  @Test(expected = StreamBoundExceededException.class)
  public void testReadLine_2() throws Exception {
    int lines = LibraryTestParameters.DATA_BOUND / LibraryTestParameters.FILE_LINE.length();
    String data = LibraryTestParameters.getData(lines);

    InputStream in = new ByteArrayInputStream(data.getBytes());

    BoundedBufferedInputStream fixture = new BoundedBufferedInputStream(in, 3); // Low bound.

    // Test reading lines over bounds.
    String result = fixture.readLine();
    assertNull(result);

    fixture.close();
  }
}