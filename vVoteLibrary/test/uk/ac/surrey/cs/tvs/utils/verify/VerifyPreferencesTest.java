/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.verify;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.json.JSONArray;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>VerifyPreferencesTest</code> contains tests for the class <code>{@link VerifyPreferences}</code>.
 */
public class VerifyPreferencesTest {

  /**
   * Run the PrefsVerification checkAllRacePreferences(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAllRacePreferences_1() throws Exception {
    PrefsVerification result = VerifyPreferences.checkAllRacePreferences(new JSONArray(),LibraryTestParameters.getInstance().getRaceDefs());
    assertNotNull(result);
    assertEquals(PrefsVerification.INVALID, result);
  }

  /**
   * Run the PrefsVerification checkAllRacePreferences(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAllRacePreferences_2() throws Exception {
    PrefsVerification result = VerifyPreferences.checkAllRacePreferences(new JSONArray(LibraryTestParameters.LA_RACE_PREFERENCES),LibraryTestParameters.getInstance().getRaceDefs());
    assertNotNull(result);
    assertEquals(PrefsVerification.INVALID, result);
  }

  /**
   * Run the PrefsVerification checkAllRacePreferences(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAllRacePreferences_3() throws Exception {
    PrefsVerification result = VerifyPreferences.checkAllRacePreferences(new JSONArray(
        LibraryTestParameters.LA_LC_ATL_RACE_PREFERENCES),LibraryTestParameters.getInstance().getRaceDefs());
    assertNotNull(result);
    assertEquals(PrefsVerification.ALL_VALID, result);
  }

  /**
   * Run the PrefsVerification checkAllRacePreferences(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckAllRacePreferences_4() throws Exception {
    PrefsVerification result = VerifyPreferences.checkAllRacePreferences(new JSONArray(
        LibraryTestParameters.LA_LC_BTL_RACE_PREFERENCES),LibraryTestParameters.getInstance().getRaceDefs());
    assertNotNull(result);
    assertEquals(PrefsVerification.ALL_VALID, result);
  }

  /**
   * Run the PrefsVerification checkPrefs(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckPrefs_1() throws Exception {
    PrefsVerification result = VerifyPreferences.checkPrefs(null);
    assertNotNull(result);
    assertEquals(PrefsVerification.INVALID, result);
  }

  /**
   * Run the PrefsVerification checkPrefs(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckPrefs_2() throws Exception {
    PrefsVerification result = VerifyPreferences.checkPrefs(new JSONArray());
    assertNotNull(result);
    assertEquals(PrefsVerification.EMPTY, result);
  }

  /**
   * Run the PrefsVerification checkPrefs(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckPrefs_3() throws Exception {
    PrefsVerification result = VerifyPreferences.checkPrefs(new JSONArray(LibraryTestParameters.EMPTY_PREFERENCES));
    assertNotNull(result);
    assertEquals(PrefsVerification.EMPTY, result);
  }

  /**
   * Run the PrefsVerification checkPrefs(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckPrefs_4() throws Exception {
    PrefsVerification result = VerifyPreferences.checkPrefs(new JSONArray(LibraryTestParameters.INVALID_PREFERENCES));
    assertNotNull(result);
    assertEquals(PrefsVerification.INVALID, result);
  }

  /**
   * Run the PrefsVerification checkPrefs(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckPrefs_5() throws Exception {
    PrefsVerification result = VerifyPreferences.checkPrefs(new JSONArray(LibraryTestParameters.DUPLICATE_PREFERENCES));
    assertNotNull(result);
    assertEquals(PrefsVerification.INVALID, result);
  }

  /**
   * Run the PrefsVerification checkPrefs(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckPrefs_6() throws Exception {
    PrefsVerification result = VerifyPreferences.checkPrefs(new JSONArray(LibraryTestParameters.OUT_OF_ORDER_PREFERENCES));
    assertNotNull(result);
    assertEquals(PrefsVerification.INVALID, result);
  }

  /**
   * Run the PrefsVerification checkPrefs(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckPrefs_7() throws Exception {
    PrefsVerification result = VerifyPreferences.checkPrefs(new JSONArray(LibraryTestParameters.MULTIPLE_PREFERENCES));
    assertNotNull(result);
    assertEquals(PrefsVerification.VALID_PREF, result);
  }

  /**
   * Run the PrefsVerification checkSinglePref(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckSinglePref_1() throws Exception {
    PrefsVerification result = VerifyPreferences.checkSinglePref(null);
    assertNotNull(result);
    assertEquals(PrefsVerification.INVALID, result);
  }

  /**
   * Run the PrefsVerification checkSinglePref(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckSinglePref_2() throws Exception {
    PrefsVerification result = VerifyPreferences.checkSinglePref(new JSONArray());
    assertNotNull(result);
    assertEquals(PrefsVerification.EMPTY, result);
  }

  /**
   * Run the PrefsVerification checkSinglePref(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckSinglePref_3() throws Exception {
    PrefsVerification result = VerifyPreferences.checkSinglePref(new JSONArray(LibraryTestParameters.EMPTY_PREFERENCES));
    assertNotNull(result);
    assertEquals(PrefsVerification.EMPTY, result);
  }

  /**
   * Run the PrefsVerification checkSinglePref(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckSinglePref_4() throws Exception {
    PrefsVerification result = VerifyPreferences.checkSinglePref(new JSONArray(LibraryTestParameters.INVALID_PREFERENCES));
    assertNotNull(result);
    assertEquals(PrefsVerification.INVALID, result);
  }

  /**
   * Run the PrefsVerification checkSinglePref(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckSinglePref_5() throws Exception {
    PrefsVerification result = VerifyPreferences.checkSinglePref(new JSONArray(LibraryTestParameters.DUPLICATE_PREFERENCES));
    assertNotNull(result);
    assertEquals(PrefsVerification.INVALID, result);
  }

  /**
   * Run the PrefsVerification checkSinglePref(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckSinglePref_6() throws Exception {
    PrefsVerification result = VerifyPreferences.checkSinglePref(new JSONArray(LibraryTestParameters.SINGLE_PREFERENCES));
    assertNotNull(result);
    assertEquals(PrefsVerification.SINGLE_PREF, result);
  }

  /**
   * Run the VerifyPreferences() constructor test.
   */
  @Test
  public void testVerifyPreferences_1() throws Exception {
    VerifyPreferences result = new VerifyPreferences();
    assertNotNull(result);
  }
}