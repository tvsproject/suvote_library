/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.bls.test;

import it.unisa.dia.gas.jpbc.Element;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.KeyGeneration;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.GenerateSignatureKeyAndCSR;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSSignature;

/**
 * @author Chris Culnane
 * 
 */
public class TestKeyPairAndSign {

  /**
   * 
   */
  public TestKeyPairAndSign() {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param args
   * @throws TVSSignatureException
   * @throws NoSuchAlgorithmException 
   * @throws KeyGenerationException 
   */
  public static void main(String[] args) throws TVSSignatureException, NoSuchAlgorithmException, KeyGenerationException {
    CryptoUtils.initProvider();
    System.out.println(System.getProperty("java.library.path"));
    BLSKeyPair kp = BLSKeyPair.generateKeyPair();
    TVSSignature sig = new TVSSignature(kp.getPrivateKey());
    sig.update("Hello, this is some test");
    byte[] sigdata = sig.sign();
    Element elem = BLSSignature.getSignatureElement(sigdata);
    for (int i = 0; i < 10; i++) {
      BLSSignature blsSig = BLSSignature.getInstance("SHA1");
      blsSig.initVerify(kp.getPublicKey());
      blsSig.update("Hello, this is some test");
      long start = System.currentTimeMillis();
      
      //TVSSignature verify = new TVSSignature(kp.getPublicKey(), false);
     
      System.out.println(blsSig.verify(elem,false));
      long end = System.currentTimeMillis();
      System.out.println(end - start);
    }
//    KeyPair ecKP =GenerateSignatureKeyAndCSR.generateKeyPair(SignatureType.ECDSA);
//    TVSSignature t = new TVSSignature(SignatureType.ECDSA,ecKP.getPrivate());
//    t.update("Hello, this is some test");
//    byte[] ecsig = t.sign();
//    for (int i = 0; i < 100; i++) {
//      long start = System.currentTimeMillis();
//      TVSSignature testSig = new TVSSignature(SignatureType.ECDSA,ecKP.getPublic(),false);
//      testSig.update("Hello, this is some test");
//       System.out.println(testSig.verify(ecsig));
//      long end = System.currentTimeMillis();
//      System.out.println(end - start);
//    }
    
    
  }

}
