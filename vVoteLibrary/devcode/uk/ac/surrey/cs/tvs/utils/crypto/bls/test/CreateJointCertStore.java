/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.bls.test;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;


/**
 * @author Chris Culnane
 *
 */
public class CreateJointCertStore {

  /**
   * 
   */
  public CreateJointCertStore() {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param args
   * @throws KeyStoreException 
   * @throws IOException 
   * @throws CertificateException 
   * @throws NoSuchAlgorithmException 
   */
  public static void main(String[] args) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
    // TODO Auto-generated method stub
    TVSKeyStore jointKeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    
    TVSKeyStore peer1KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer1KeyStore.load("./keyoutput/Peer1_public.bks",null);
    
    TVSKeyStore peer2KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer2KeyStore.load("./keyoutput/Peer2_public.bks",null);
    
    TVSKeyStore peer3KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer3KeyStore.load("./keyoutput/Peer3_public.bks",null);
    
    TVSKeyStore peer4KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer4KeyStore.load("./keyoutput/Peer4_public.bks",null);
    
    TVSKeyStore peer5KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer5KeyStore.load("./keyoutput/Peer5_public.bks",null);
    
    jointKeyStore.addBLSKeyStoreEntries(peer1KeyStore);
    jointKeyStore.addBLSKeyStoreEntries(peer2KeyStore);
    jointKeyStore.addBLSKeyStoreEntries(peer3KeyStore);
    jointKeyStore.addBLSKeyStoreEntries(peer4KeyStore);
    jointKeyStore.addBLSKeyStoreEntries(peer5KeyStore);
    jointKeyStore.store("./keyoutput/certs.bks");
    
    
    
    
  }

}
