/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.bls.test;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;


/**
 * @author Chris Culnane
 *
 */
public class TestKeyExtraction {

  
  /**
   * @param args
   * @throws TVSKeyStoreException 
   */
  public static void main(String[] args) throws TVSKeyStoreException {
    // TODO Auto-generated method stub
    TVSKeyStore.extractBLSKeysFromPKCS12("./data/bls_node1.p12", "Hello", "Peer1_SigningSK2", "./keyoutput/Peer1_public.bks", "./keyoutput/Peer1_private.bks");
    TVSKeyStore.extractBLSKeysFromPKCS12("./data/bls_node2.p12", "Hello", "Peer2_SigningSK2", "./keyoutput/Peer2_public.bks", "./keyoutput/Peer2_private.bks");
    TVSKeyStore.extractBLSKeysFromPKCS12("./data/bls_node3.p12", "Hello", "Peer3_SigningSK2", "./keyoutput/Peer3_public.bks", "./keyoutput/Peer3_private.bks");
    TVSKeyStore.extractBLSKeysFromPKCS12("./data/bls_node4.p12", "Hello", "Peer4_SigningSK2", "./keyoutput/Peer4_public.bks", "./keyoutput/Peer4_private.bks");
    TVSKeyStore.extractBLSKeysFromPKCS12("./data/bls_node5.p12", "Hello", "Peer5_SigningSK2", "./keyoutput/Peer5_public.bks", "./keyoutput/Peer5_private.bks");
  }

}
