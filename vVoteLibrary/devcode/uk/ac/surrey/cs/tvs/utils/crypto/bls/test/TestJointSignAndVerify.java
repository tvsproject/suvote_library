/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.bls.test;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;


/**
 * @author Chris Culnane
 *
 */
public class TestJointSignAndVerify {

  /**
   * 
   */
  public TestJointSignAndVerify() {
    // TODO Auto-generated constructor stub
  }

  
  /**
   * @param args
   * @throws KeyStoreException 
   * @throws IOException 
   * @throws CertificateException 
   * @throws NoSuchAlgorithmException 
   * @throws TVSSignatureException 
   * @throws JSONException 
   */
  public static void main(String[] args) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, TVSSignatureException, JSONException {
    // TODO Auto-generated method stub
    JSONArray sigs = new JSONArray();
    String testMessage = "This is a test";
    TVSKeyStore peer1KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer1KeyStore.load("./keyoutput/Peer1_private.bks",null);
    
    
    TVSKeyStore peer2KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer2KeyStore.load("./keyoutput/Peer2_private.bks",null);
    
    TVSKeyStore peer3KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer3KeyStore.load("./keyoutput/Peer3_private.bks",null);
    
    TVSKeyStore peer4KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer4KeyStore.load("./keyoutput/Peer4_private.bks",null);
    
    TVSKeyStore peer5KeyStore =TVSKeyStore.getInstance(KeyStore.getDefaultType());
    peer5KeyStore.load("./keyoutput/Peer5_private.bks",null);
    
    sigs.put(createSignature("Peer1_SigningSK2",peer1KeyStore,testMessage));
    sigs.put(createSignature("Peer2_SigningSK2",peer2KeyStore,testMessage));
    sigs.put(createSignature("Peer3_SigningSK2",peer3KeyStore,testMessage));
    sigs.put(createSignature("Peer5_SigningSK2",peer5KeyStore,testMessage));
    
    TVSKeyStore jointStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    jointStore.load("./keyoutput/certs.bks",null);
    //TVSSignature verify = new TVSSignature(SignatureType.BLS,jointStore);
    //verify.update(testMessage);
    //System.out.println(verify.verify(sigs, "").toString());
    
    TVSSignature verifyJoin = new TVSSignature(SignatureType.BLS,jointStore);
    verifyJoin.update(testMessage);
    JSONObject output = verifyJoin.verifyAndCombine(sigs, "", 4, 5,true,true);
    System.out.println(output.toString());
    
    
    
    TVSSignature jointVerify = new TVSSignature(SignatureType.BLS,jointStore.getBLSCertificate("WBB"));
    jointVerify.update(testMessage);
    System.out.println(jointVerify.verify(output.getString("combined"),EncodingType.BASE64));
  }

  public static JSONObject createSignature(String alias, TVSKeyStore keyStore,String message) throws JSONException, TVSKeyStoreException, TVSSignatureException{
    JSONObject obj = new JSONObject();
    obj.put("WBBID", alias);
    TVSSignature sig1 = new TVSSignature(keyStore.getBLSPrivateKey(alias));
    sig1.update(message);
    obj.put("WBBSig",sig1.signAndEncode(EncodingType.BASE64));
    return obj;
  }
}
