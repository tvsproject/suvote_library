/*
 * Copyright (C) 2008-2009 TVS Group - University of Surrey This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version. See
 * <http://www.fsf.org/copyleft/gpl.txt>.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 */
package uk.ac.surrey.cs.tvs.utils.crypto;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * To prove that log v = log w, where v = g_1^x and w = g_2^x, let: z = random in Z_q a = g_1^z b = g_2^z c = hash(v,w,a,b) r = (z +
 * cx) mod q The proof is (a,b,c,r). To verify, check that g_1^r = av^c (mod p) and g_2^r = bw^c (mod p).
 */
public class EqualityDiscLog_EC {

  public final ECPoint    g1;
  public final ECPoint    g2;

  public final ECPoint    v;
  public final ECPoint    w;

  public final ECPoint    a;
  public final ECPoint    b;
  public final BigInteger c;
  public final BigInteger r;

  public EqualityDiscLog_EC(ECPoint g1, ECPoint g2, ECPoint v, ECPoint w, ECPoint a, ECPoint b, BigInteger c, BigInteger r) {
    this.g1 = g1;
    this.g2 = g2;
    this.v = v;
    this.w = w;
    this.a = a;
    this.b = b;
    this.c = c;
    this.r = r;
  }

  public EqualityDiscLog_EC(JSONObject proof, ECUtils group) throws JSONException {
    this.g1 = group
        .getParams()
        .getCurve()
        .createPoint(new BigInteger(proof.getJSONObject("g1").getString("x"), 16),
            new BigInteger(proof.getJSONObject("g1").getString("y"), 16), false);
    this.g2 = group
        .getParams()
        .getCurve()
        .createPoint(new BigInteger(proof.getJSONObject("g2").getString("x"), 16),
            new BigInteger(proof.getJSONObject("g2").getString("y"), 16), false);
    this.v = group
        .getParams()
        .getCurve()
        .createPoint(new BigInteger(proof.getJSONObject("v").getString("x"), 16),
            new BigInteger(proof.getJSONObject("v").getString("y"), 16), false);
    this.w = group
        .getParams()
        .getCurve()
        .createPoint(new BigInteger(proof.getJSONObject("w").getString("x"), 16),
            new BigInteger(proof.getJSONObject("w").getString("y"), 16), false);
    this.a = group
        .getParams()
        .getCurve()
        .createPoint(new BigInteger(proof.getJSONObject("a").getString("x"), 16),
            new BigInteger(proof.getJSONObject("a").getString("y"), 16), false);
    this.b = group
        .getParams()
        .getCurve()
        .createPoint(new BigInteger(proof.getJSONObject("b").getString("x"), 16),
            new BigInteger(proof.getJSONObject("b").getString("y"), 16), false);
    this.c = new BigInteger(proof.getString("c"), 16);
    this.r = new BigInteger(proof.getString("r"), 16);
  }

  public JSONObject getAsJSON() throws JSONException {
    JSONObject proof = new JSONObject();
    proof.put("g1", ECUtils.ecPointToJSON(this.g1));
    proof.put("g2", ECUtils.ecPointToJSON(this.g2));
    proof.put("v", ECUtils.ecPointToJSON(this.v));
    proof.put("w", ECUtils.ecPointToJSON(this.w));
    proof.put("a", ECUtils.ecPointToJSON(this.a));
    proof.put("b", ECUtils.ecPointToJSON(this.b));
    proof.put("c", this.c.toString(16));
    proof.put("r", this.r.toString(16));
    return proof;
  }

  public static EqualityDiscLog_EC constructProof(ECUtils grp, ECPoint g1, ECPoint g2, BigInteger x)
      throws NoSuchAlgorithmException {

    ECPoint v = g1.multiply(x);
    ECPoint w = g2.multiply(x);
    BigInteger z = grp.getRandomInteger(grp.getOrderUpperBound(), new SecureRandom());

    ECPoint a = g1.multiply(z);
    ECPoint b = g2.multiply(z);

    MessageDigest md = MessageDigest.getInstance("SHA");
    md.update(v.getEncoded());
    md.update(w.getEncoded());
    md.update(a.getEncoded());
    md.update(b.getEncoded());
    BigInteger c = new BigInteger(1, md.digest());
    BigInteger r = z.add(c.multiply(x)).mod(grp.getOrderUpperBound());
    return new EqualityDiscLog_EC(g1, g2, v, w, a, b, c, r);
  }

  public static EqualityDiscLog_EC constructProof(ECUtils grp, ECPoint g1, ECPoint g2, ECPoint key, ECPoint pdec, BigInteger x)
      throws NoSuchAlgorithmException {

    ECPoint v = key;
    ECPoint w = pdec;
    BigInteger z = grp.getRandomInteger(grp.getOrderUpperBound(), new SecureRandom());

    ECPoint a = g1.multiply(z);
    ECPoint b = g2.multiply(z);

    MessageDigest md = MessageDigest.getInstance("SHA");
    md.update(v.getEncoded());
    md.update(w.getEncoded());
    md.update(a.getEncoded());
    md.update(b.getEncoded());
    BigInteger c = new BigInteger(1, md.digest());
    BigInteger r = z.add(c.multiply(x)).mod(grp.getOrderUpperBound());
    return new EqualityDiscLog_EC(g1, g2, v, w, a, b, c, r);
  }

  public static boolean verify(ECUtils grp, EqualityDiscLog_EC E) {
    return E.g1.multiply(E.r).equals(E.v.multiply(E.c).add(E.a)) && E.g2.multiply(E.r).equals(E.w.multiply(E.c).add(E.b));
  }

}