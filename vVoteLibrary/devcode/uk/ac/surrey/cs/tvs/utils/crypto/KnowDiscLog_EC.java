/*
 * Copyright (C) 2008-2009 TVS Group - University of Surrey This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version. See
 * <http://www.fsf.org/copyleft/gpl.txt>.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 */
package uk.ac.surrey.cs.tvs.utils.crypto;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Proof that an entity knows x in v = g^x.
 */

/*
 * p,q,g are the parameters of the ElGamal (not included here). z = random in Z_q a = g^z mod p c = hash(v,a) r = (z + cx) mod q
 * 
 * To verify proof, check that g^r = av^c (mod p)
 */

public class KnowDiscLog_EC {

  ECPoint    a;
  BigInteger c;
  BigInteger r;
  ECPoint    v;

  public KnowDiscLog_EC(ECPoint a, BigInteger c, BigInteger r, ECPoint v) {
    this.a = a;
    this.c = c;
    this.r = r;
    this.v = v;
  }

  public KnowDiscLog_EC(JSONObject proof, ECUtils group) throws JSONException {
    this.a = group
        .getParams()
        .getCurve()
        .createPoint(new BigInteger(proof.getJSONObject("a").getString("x"), 16),
            new BigInteger(proof.getJSONObject("a").getString("y"), 16), false);
    this.c = new BigInteger(proof.getString("c"), 16);
    this.r = new BigInteger(proof.getString("r"), 16);
    this.v = group
        .getParams()
        .getCurve()
        .createPoint(new BigInteger(proof.getJSONObject("v").getString("x"), 16),
            new BigInteger(proof.getJSONObject("v").getString("y"), 16), false);
  }

  public JSONObject getAsJSON() throws JSONException {
    JSONObject proof = new JSONObject();
    proof.put("a", ECUtils.ecPointToJSON(this.a));
    proof.put("c", this.c.toString(16));
    proof.put("r", this.r.toString(16));
    proof.put("v", ECUtils.ecPointToJSON(this.v));
    return proof;
  }

  public static KnowDiscLog_EC createProof(ECUtils grp, BigInteger x, ECPoint v) throws NoSuchAlgorithmException {
    ECPoint g = grp.getG();
    BigInteger z = grp.getRandomInteger(grp.getOrderUpperBound(), new SecureRandom());
    ECPoint a = g.multiply(z);
    MessageDigest md = MessageDigest.getInstance("SHA");
    md.update(v.getEncoded());
    md.update(a.getEncoded());
    BigInteger c = new BigInteger(1, md.digest());
    BigInteger r = z.add(c.multiply(x)).mod(grp.getOrderUpperBound());
    return new KnowDiscLog_EC(a, c, r, v);
  }

  public static boolean verifyProof(ECUtils grp, KnowDiscLog_EC E) {
    ECPoint u = grp.getG().multiply(E.r);
    ECPoint w = E.v.multiply(E.c).add(E.a);
    return u.equals(w);
  }

}
